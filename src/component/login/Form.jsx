import React, { useState } from 'react';
//import headerBar from './headerBar';
import Footer from '../footer';
import TopBar from '../topBar';
import Content from '../content';
import SideBar from '../sideBar';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUser, faKey } from '@fortawesome/free-solid-svg-icons';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { Button } from 'reactstrap';
import { Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import { Route, Redirect, useHistory } from "react-router-dom";








const Form = () => {

    let history = useHistory();


    // const handleSubmit = (e) => {
    //     e.preventDefault();
    //     value.setStudent(prevStudent => [...prevStudent, {
    //         name: name,
    //         lastName: lastName,
    //         lastName2: lastName2,
    //         email: email,
    //         birthday: birthday,
    //         semester: semester,
    //         gender: gender,
    //         club: club,
    //         curp: curp,
    //         bloodType: bloodType,
    //         allergies: allergies,
    //         diseases: diseases
    //     }]);
    //     //console.log(value.student)
    // }
    function swalAlert(e) {
        Swal.fire({
            background: 'linear-gradient(to right, 	#9370DB, white)',
            color: 'white',
            title: 'Correcto',
            text: "Has iniciado sesión Correctamente!",
            icon: 'success',
            showCancelButton: false,
            confirmButtonColor: '#9400D3',
            confirmButtonText: 'Continuar',
        }).then((result) => {
            if (result.value) {
                history.push('/');
                Swal.fire({
                    background: 'linear-gradient(to right, 	#9370DB, white)',
                    title: 'Correcto',
                    text: "Bienvenido a SunnyPass!",
                    icon: 'info',
                }
                )
            } else {
                history.push('/register')
            }
        })
    }

    return (
        <>
            <div className='main'>
                <TopBar />
                <SideBar />
                <div className='contentX'>
                    <div className='bg'></div>
                    <div className='container1X'>
                        <div style={{ color: 'white', backgroundColor: 'purple', height: '7.5vh', justifyContent: 'center', paddingTop: '1.5vh', paddingBotton: '-0.5vh', fontSize: '24px' }}>Ingrese sus datos</div>
                        <div className='containerRow1X'>
                            <div>
                                <FontAwesomeIcon icon={faUser} style={{ marginLeft: '-2vw', marginRight: '0.2vw', color: 'black' }} />
                                <i class="fas fa-user"></i>
                                <input
                                    id="user"
                                    type="text"
                                    className="form-control"
                                    name="user"
                                    placeholder="Usuario"
                                />
                            </div>
                        </div>

                        <div className='containerRow1X'>
                            <div>
                                <FontAwesomeIcon icon={faKey} style={{ marginLeft: '-2vw', marginRight: '0.2vw', color: 'black' }} />
                                <i class="fas fa-key"></i>
                                <input
                                    id="password"
                                    type="password"
                                    className="form-control"
                                    name="password"
                                    placeholder="Contraseña"
                                />
                            </div>
                        </div>

                        <div className='containerRow1X'>
                            <Button className='buttonA' onClick={e => (swalAlert(e))}>
                                <span class="fas fa-user" style={{ marginRight: '0.3vw' }}></span>
                            INICIAR
                            </Button>
                            <Link to='/'>
                                <Button className='buttonA'>VOLVER</Button>
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
            <Footer />
        </>
    );
}

export default Form;