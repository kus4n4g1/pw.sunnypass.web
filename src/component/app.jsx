import React from 'react';
//import headerBar from './headerBar';
// import Footer from './footer';
// import TopBar from './topBar';
// import Content from './content';
// import SideBar from './sideBar';
import Home from './home';
import Register from './register';
import GetApp from './getApp';
import Login from './login';
import AboutUs from './aboutUs';
import Contact from './contact';
import Options from './options';
import Sugest from './sugest';
import Calculator from './calculator';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';


const App = () => {

    return (
        <Router>
            <Switch>
                <Route path="/" exact component={Home} />
                <Route path="/register" exact component={Register} />
                <Route path="/getApp" component={GetApp} />
                <Route path="/login" component={Login} />
                <Route path="/aboutUs" exact component={AboutUs} />
                <Route path="/contact" exact component={Contact} />
                <Route path="/options" component={Options} />
                <Route path="/sugest" component={Sugest} />
                <Route path="/calculate" component={Calculator} />
                <Route path="/external" component={() => { window.location = 'http://www.google.com/'; return null; }} />


                {/* <Route path="/dashboard" exact component={Dashboard} />
                <Route path="/dashboard/contact" component={Contact} />
                <Route path="/dashboard/register" component={RegisterX} />
                <Route path="/dashboard/taller" component={Taller} />
                <Route path="/dashboard/club" component={Club} />
                <Route path="/dashboard/student" component={Student} />
                <Route path="/dashboard/teacher" component={Teacher} />
                <Route path="/dashboard/support" component={Support} />
                <Route path="/dashboard/agenda" component={Agenda} />
                <Route path="/dashboard/admin/student" exact component={AdminStudent} />
                <Route path="/dashboard/admin/teacher" exact component={AdminTeacher} />
                <Route path="/dashboard/admin/taller" exact component={AdminTaller} />
                <Route path="/dashboard/admin/club" exact component={AdminClub} />
                <Route path="/dashboard/admin/user" exact component={User} />
                <Route path="/external" component={() => { window.location = 'http://enso.creson.edu.mx/'; return null; }} /> */}
            </Switch>
        </Router>

    );
}

export default App;