import React, { useState } from 'react';
//import headerBar from './headerBar';
import Footer from '../footer';
import TopBar from '../topBar';
import Content from '../content';
import SideBar from '../sideBar';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUser, faAddressBook, faMale, faFemale, faBirthdayCake, faBaby } from '@fortawesome/free-solid-svg-icons';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { Button } from 'reactstrap';
import { Link } from 'react-router-dom';
import Calendar from './calendar';
import moment from 'moment';
import Swal from 'sweetalert2';
import { Route, Redirect, useHistory } from "react-router-dom";








const Form = () => {

    const [birthday, setBirthday] = useState(null);
    let history = useHistory();

    const dateB = Date.now.toString();
    console.log(dateB);
    moment.locale('es');
    var formattedDate = moment.locale;

    // const handleSubmit = (e) => {
    //     e.preventDefault();
    //     value.setStudent(prevStudent => [...prevStudent, {
    //         name: name,
    //         lastName: lastName,
    //         lastName2: lastName2,
    //         email: email,
    //         birthday: birthday,
    //         semester: semester,
    //         gender: gender,
    //         club: club,
    //         curp: curp,
    //         bloodType: bloodType,
    //         allergies: allergies,
    //         diseases: diseases
    //     }]);
    //     //console.log(value.student)
    // }
    function swalAlert(e) {
        Swal.fire({
            background: 'linear-gradient(to right, 	#9370DB, white)',
            color: 'white',
            title: 'Correcto',
            text: "Te has inscrito a SunnyPass correctamente!",
            icon: 'success',
            showCancelButton: true,
            confirmButtonColor: '#9400D3',
            cancelButtonColor: '#3085d6',
            confirmButtonText: 'Volver a Inicio',
            cancelButtonText: 'Seguir registrando'
        }).then((result) => {
            if (result.value) {
                history.push('/');
                Swal.fire({
                    background: 'linear-gradient(to right, 	#9370DB, white)',
                    title: 'Correcto',
                    text: "Bienvenido a SunnyPass!",
                    icon: 'info',
                }
                )
            } else {
                history.push('/register')
            }
        })
    }

    return (
        <>
            <div className='main'>
                <TopBar />
                <SideBar />
                <div className='contentX'>
                    <div className='bg'></div>
                    <div className='container1X'>
                        <div style={{ color: 'white', backgroundColor: 'purple', height: '7.5vh', justifyContent: 'center', paddingTop: '1.5vh', paddingBotton: '-0.5vh', fontSize: '24px' }}>Ingrese sus datos</div>
                        <div className='containerRow1X'>
                            <div>
                                <FontAwesomeIcon icon={faUser} style={{ marginLeft: '-2vw', marginRight: '0.2vw', color: 'black' }} />
                                <i class="fas fa-user"></i>
                                <input
                                    id="name"
                                    type="text"
                                    className="form-control"
                                    name="name"
                                    placeholder="Nombre"
                                />
                            </div>
                        </div>

                        <div className='containerRow1X'>
                            <div>
                                <FontAwesomeIcon icon={faMale} style={{ marginLeft: '-2vw', marginRight: '0.2vw', color: 'black' }} />
                                <i class="fas fa-user"></i>
                                <input
                                    id="lastName"
                                    type="text"
                                    className="form-control"
                                    name="lastName"
                                    placeholder="Paterno"
                                />
                            </div>
                        </div>
                        <div className='containerRow1X'>
                            <div>
                                <FontAwesomeIcon icon={faFemale} style={{ marginLeft: '-2vw', marginRight: '0.2vw', color: 'black' }} />
                                <i class="fas fa-user"></i>
                                <input
                                    id="lastName2"
                                    type="text"
                                    className="form-control"
                                    name="lastName2"
                                    placeholder="Materno"
                                />
                            </div>
                        </div>
                        <div className='containerRow1X'>
                            <div>
                                <FontAwesomeIcon icon={faAddressBook} style={{ marginLeft: '-2vw', marginRight: '0.2vw', color: 'black' }} />
                                <i class="fas fa-user"></i>
                                <input
                                    id="address"
                                    type="text"
                                    className="form-control"
                                    name="address"
                                    placeholder="Domicilio"
                                />
                            </div>
                        </div>
                        <div className='containerRow1X'>
                            <div>
                                <FontAwesomeIcon icon={faBirthdayCake} style={{ marginLeft: '-2vw', marginRight: '0.2vw', color: 'black' }} />
                                <i class="fas fa-user"></i>
                                <Calendar
                                    id="birth"
                                    type="text"
                                    className="form-control"
                                    name="birth"
                                    placeholder="Nacimiento"
                                    value={formattedDate}
                                    birthday={birthday}
                                    setBirthday={setBirthday}
                                // onChange={data => setBirthday(data.value)}
                                />
                            </div>
                        </div>
                        <div className='containerRow1X'>
                            <div>
                                <FontAwesomeIcon icon={faBaby} style={{ marginLeft: '-2vw', marginRight: '0.2vw', color: 'black' }} />
                                <i class="fas fa-user"></i>
                                <input
                                    id="age"
                                    type='number'
                                    className="form-control"
                                    name="age"
                                    placeholder="Edad"
                                />
                            </div>
                        </div>
                        <div className='containerRow1X'>
                            <Button className='buttonA' onClick={e => (swalAlert(e))}>
                                <span class="fas fa-user" style={{ marginRight: '0.3vw' }}></span>
                            REGISTRAR
                            </Button>
                            <Link to='/'>
                                <Button className='buttonA'>VOLVER</Button>
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
            <Footer />
        </>
    );
}

export default Form;