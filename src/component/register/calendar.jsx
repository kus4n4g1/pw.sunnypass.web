import React from 'react';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import propTypes from 'prop-types';

const Calendar = (props) => {

    const {
        birthday,
        setBirthday
    } = props;

    // const [selectedDate, setSelectedDate] = useState(null);

    // const handleChange = (selectedOption) => {
    //     console.log('It will never happen');
    // };
    console.log("esto es birthday", birthday);

    return (

        <DatePicker
            className='calendarStyle'
            style={{ fontSize: '16px', marginRight: '3vw' }}
            selected={birthday}
            dateFormat='dd/MM/yyyy'
            maxDate={new Date()}
            onChange={date => setBirthday(date)}
            placeholderText='Fecha de Nacimiento'
            showYearDropdown
            scrollableYearDropdown
        />

    );

}
Calendar.propTypes = {
    birthday: propTypes.any,
    setBirthday: propTypes.func
};

export default Calendar;

