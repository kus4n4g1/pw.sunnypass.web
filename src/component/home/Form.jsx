import React from 'react';
//import headerBar from './headerBar';
import Footer from '../footer';
import TopBar from '../topBar';
import Content from '../content';
import SideBar from '../sideBar';


const Form = () => {

    return (
        <>
            <div className='main'>
                <TopBar />
                <SideBar />
                <Content />
            </div>
            <Footer />
        </>
    );
}

export default Form;

