import React from 'react';
import { Button, Input } from 'reactstrap';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
    faKiwiBird,
    faGlobeAmericas,
    faBook,
    faVideo,
    faInfo,
    faPhotoVideo

} from '@fortawesome/free-solid-svg-icons';
import { Link } from 'react-router-dom';
import { Route, Redirect, useHistory } from "react-router-dom";
import Swal from 'sweetalert2';

const Form = () => {

    let history = useHistory();

    function swalAlert(e) {

        if (e.key === 'Enter') {
            Swal.fire({
                background: 'linear-gradient(to right, 	#9370DB, white)',
                color: 'white',
                title: 'Correcto',
                text: "A continuación, se te dirige a las ofertas!",
                icon: 'success',
                showCancelButton: false,
                confirmButtonColor: '#9400D3',
                confirmButtonText: 'Continuar',
            }).then((result) => {
                if (result.value) {
                    history.push('/getApp');
                    Swal.fire({
                        background: 'linear-gradient(to right, 	#9370DB, white)',
                        title: 'Correcto',
                        text: "Momentáneamente la busqueda es manual. Sitio en Construcción. Disculpe las molestias!",
                        icon: 'info',
                    }
                    )
                } else {
                    history.push('/register')
                }
            })
        }

    }

    function swalAlert2(e) {

        Swal.fire({
            background: 'linear-gradient(to right, 	#9370DB, white)',
            color: 'white',
            title: 'Correcto',
            text: "A continuación, se te dirige a las ofertas!",
            icon: 'success',
            showCancelButton: false,
            confirmButtonColor: '#9400D3',
            confirmButtonText: 'Continuar',
        }).then((result) => {
            if (result.value) {
                history.push('/getApp');
                Swal.fire({
                    background: 'linear-gradient(to right, 	#9370DB, white)',
                    title: 'Correcto',
                    text: "Momentáneamente la busqueda es manual. Sitio en Construcción. Disculpe las molestias!",
                    icon: 'info',
                }
                )
            } else {
                history.push('/register')
            }
        })


    }

    return (
        <div className='footer'>
            <div style={{ display: 'flex', flexDirection: 'column', justifyContent: 'center' }}>
                {/* <span class="fas fa-university" style={{ marginRight: '0.3vw' }}></span> */}
                Copyright © 2021, Sunny O. Orukwo Escalante
                <div style={{ display: 'flex', flexDirection: 'row' }}>
                    <Input style={{ width: '95%' }} placeholder='Escriba su búsqueda' onSubmit={(e) => swalAlert(e)}
                        onKeyUp={(e) => swalAlert(e)}></Input>
                    <FontAwesomeIcon icon={faSearch} style={{ width: '44px', height: '44px', margin: '1vh 1vw 0vh 1vw' }} onClick={e => swalAlert2(e)} />
                </div>
                <div style={{ display: 'flex', flexDirection: 'row', color: 'white' }}>
                    <Link to='/'>
                        <div>Zoho Inicio</div>
                    </Link>
                    <div>|</div>
                    <Link to='/sugest'>
                        <div>Comuniquese con nosotros</div>
                    </Link>

                    <div>|</div>
                    <Link to='/aboutUs'>
                        <div>Seguridad</div>
                    </Link>
                    <div>|</div>
                    <Link to='/sugest'>
                        <div>Quejas</div>
                    </Link>
                    <div>|</div>
                    <Link to='/aboutUs'>
                        <div>Politica Anti-Spam</div>
                    </Link>
                    <div>|</div>
                    <Link to='/aboutUs'>
                        <div>Términos</div>
                    </Link>
                    <div>|</div>
                    <Link to='/contact'>
                        <div>Privacidad</div>
                    </Link>
                    <div>|</div>
                    <Link to='/aboutUs'>
                        <div>Cookies</div>
                    </Link>
                    <div>|</div>
                    <Link to='/aboutUs'>
                        <div>Rgpd</div>
                    </Link>
                    <div>|</div>
                    <Link to='/aboutUs'>
                        <div>Política de Abuso</div>
                    </Link>
                </div>
                <div>2021 Zoho Corporations, Copia no Autorizada del Sitio</div>
                <div style={{ display: 'flex', flexDirection: 'row' }}>
                    <Link to='/'>
                        <FontAwesomeIcon icon={faGlobeAmericas} style={{ width: '20px', height: '20px', margin: '0.1vh 1vw 0vh 0vw', color: '#4B0082' }} />
                    </Link>
                    <div>Español</div>
                </div>
                <div style={{ display: 'flex', flexDirection: 'row' }}>
                    <Link to='/external'>
                        <FontAwesomeIcon icon={faKiwiBird} style={{ width: '20px', height: '20px', margin: '0.1vh  0.3vw 0vh 0vw' }} />
                        <FontAwesomeIcon icon={faBook} style={{ width: '20px', height: '20px', margin: '0.1vh  0.3vw 0vh 0vw' }} />
                        <FontAwesomeIcon icon={faVideo} style={{ width: '20px', height: '20px', margin: '0.1vh  0.3vw 0vh 0vw' }} />
                        <FontAwesomeIcon icon={faInfo} style={{ width: '20px', height: '20px', margin: '0.1vh  0.3vw 0vh 0vw' }} />
                        <FontAwesomeIcon icon={faPhotoVideo} style={{ width: '20px', height: '20px', margin: '0.1vh 0.3vw 0vh 0vw' }} />
                    </Link>
                </div>
            </div>
        </div>
    );
}

export default Form;