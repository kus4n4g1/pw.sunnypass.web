import React, { useState, useToggle } from 'react';
import { Link } from 'react-router-dom';
import { Button, Input } from 'reactstrap';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
    faUser,
    faBook,
    faDesktop,
    faCube,
    faRecycle,
    faHandsHelping,
    faPeopleArrows,
    faRunning,
    faClipboardCheck,
    faBacon,
    faCalendar,
    faFolder,
    faSign,
    faSchool,
    faPersonBooth,
    faSurprise,
    faProcedures,
    faSatellite,
    faNewspaper,
    faMapSigns,
    faBug,
    faReceipt,
    faReply,
    faWalking,
    faFolderPlus,
    faPen,
    faShapes,
    faVideo,
    faPaintBrush,
    faBoxOpen,
    faVideoSlash,
    faNotesMedical,
    faDatabase,
    faUserNinja,
    faJedi,
    faStore,
    faFileInvoice,
    faSubscript,
    faMoneyBill,
    faCheck,
    faUserCheck,
    faPeopleCarry,
    faCode,
    faCommentDots,
    faUserFriends,
    faMobile,
    faMagic,
    faLaptop,
    faDownload,
    faCloud,
    faPaw,
    faHatWizard,
    faHammer,
    faGlasses,
    faStarOfLife,
    faStickyNote,
    faFire,
    faGift,
    faChartBar,
    faMedkit,
    faProjectDiagram

} from '@fortawesome/free-solid-svg-icons';
import { FaInfinity, FaMailBulk, FaCreativeCommons } from 'react-icons/fa';

const Form = () => {
    const [count, setCount] = useState(0);
    const [isOn, toggleIsOn] = useState();
    var x = document.getElementById("myDIV");

    // function myFunction(e) {
    //     console.log("Entra la función: ", e);
    //     setHidden(false);
    //     console.log("Valor de hidden: ", hidden);
    //     console.log("Var X: ", x)
    //     if (hidden === false) {
    //         return (
    //             <h3 style={{ backgroundColor: 'black' }}>Hola que hace</h3>
    //         );
    //     }
    //     if (x.style.display === "none") {
    //         x.style.display = "block";
    //     } else {
    //         x.style.display = "none";
    //     }

    // }
    function myLastTry() {
        toggleIsOn(true);
        setCount(s => s + 1);
        console.log('Cuanto vale count en esta vuelta: ', count)
        if (((count >= 1) && ((count % 2) === 0))) {
            toggleIsOn(false);
        }
    }

    function isOnFunction() {
        console.log('Entró is On Function');
        if (isOn) {
            return (
                <div className='contentMenuColumn' style={{ backgroundColor: 'black', height: '270vh' }}>
                    <div className='contentMenu'>
                        <div class="content1">
                            <Link to='/register'>
                                <div className='imageControllerX' />
                            </Link>
                            <div>Simplemente</div>
                        </div>

                        <div class="content2">
                            <div className='imageCatalogX' onClick={myLastTry} />
                            <div>El Mejor Catálogo</div>
                        </div>
                        <div class="content3">
                            <Link to='/getApp'>
                                <div className='imageAppX' />
                            </Link>
                            <div>Del Mercado</div>
                        </div>

                    </div>
                    {/* <div className='contentMenu'>
                        <div class="content1">
                            <Link to='/register'>
                                <div className='imageControllerX' />
                            </Link>
                            <div>SunnyPass, la mejor opción en el mercado</div>
                        </div>

                        <div class="content2">
                            <div className='imageCatalogX' onClick={myLastTry} />
                            <div>Catálogo de juegos dentro de SunnyPass</div>
                        </div>
                        <div class="content3">
                            <Link to='/getApp'>
                                <div className='imageAppX' />
                            </Link>
                            <div>Consigue la App ahora</div>
                        </div>

                    </div>
                    <div className='contentMenu'>
                        <div class="content1">
                            <Link to='/register'>
                                <div className='imageControllerX' />
                            </Link>
                            <div>SunnyPass, la mejor opción en el mercado</div>
                        </div>

                        <div class="content2">
                            <div className='imageCatalogX' onClick={myLastTry} />
                            <div>Catálogo de juegos dentro de SunnyPass</div>
                        </div>
                        <div class="content3">
                            <Link to='/getApp'>
                                <div className='imageAppX' />
                            </Link>
                            <div>Consigue la App ahora</div>
                        </div>

                    </div> */}
                </div>

            )
        } else {
            return (
                <div style={{ display: 'none' }}>is OFF!!!!</div>
            )
        }

    }

    function isOffFunction() {
        return (
            <div>is OFF!!!!</div>
        )
    }

    return (
        <div className='content'>
            <div className='bg'></div>
            <div style={{ display: 'flex', flexDirection: 'column' }}>
                <div style={{ color: 'white', fontFamily: 'Roboto', fontSize: '30px', marginTop: '310vh' }}>Su trabajo de toda una vida respaldado por el nuestro</div>
                <p>Un conjunto de software único y potente para gestionar todo su negocio, el cual llega a usted a través de una empresa que tiene una visión a largo plazo de transformar la forma en la que trabaja.</p>
            </div>
            <div className='xContent' style={{ display: 'flex', flexDirection: 'row' }}>
                <div style={{ display: 'flex', flexDirection: 'row' }}>
                    <div style={{ display: 'flex', flexDirection: 'column' }}>
                        <div>
                            {/* <FontAwesomeIcon icon={faUser} style={{ marginRight: '0vw' }} /> */}
                            <FaInfinity size='70px' style={{ margin: '1vh 1vw 0vh 1vw' }} />

                        </div>
                        <div>
                            CRM
                        </div>
                    </div>
                    <div style={{ display: 'flex', flexDirection: 'column' }}>
                        <div style={{ fontSize: '24px', fontWeight: 'bold' }}>Una plataforma de CRM completa
                        </div>
                        <div style={{ height: '36%' }}>Solución de CRM integral, totalmente personalizable para empresas y negocios en crecimiento.</div>
                        <Link to='/register'>
                            <Button className='buttonA' style={{ width: '50%', marginLeft: '0%', marginTop: '5vh', marginBottom: '5vh' }}>REGISTRARSE AHORA</Button>
                        </Link>
                    </div>
                </div>
                <div style={{ display: 'flex', flexDirection: 'column' }}>
                    <div style={{ display: 'flex', flexDirection: 'row', height: '50%' }}>
                        <FaMailBulk size='50px' style={{ margin: '1vh 1vw 0vh 1vw' }} />
                        <div style={{ display: 'flex', flexDirection: 'column', width: '50%' }}>
                            <div style={{ fontSize: '18px', fontWeight: 'bold' }}>Mail</div>
                            <div style={{ fontSize: '15px' }}>Correo electrónico empresarial seguro.</div>

                            <Link to='/register'>
                                <Button className='buttonA' style={{ width: '50%', height: '5vh', marginTop: '1.5vh', marginLeft: '0%', marginBottom: '1.5vh', opacity: '0.75', backgroundColor: 'linear-gradient(to right, #4B0082, #8B008B) important!', fontSize: '12.5px' }}>REGISTRATE</Button>
                            </Link>
                        </div>
                        <FaCreativeCommons size='50px' style={{ margin: '1vh 1vw 0vh 1vw' }} />
                        <div style={{ display: 'flex', flexDirection: 'column', width: '50%' }}>
                            <div style={{ fontSize: '18px', fontWeight: 'bold' }}>Creator</div>
                            <div style={{ fontSize: '15px' }}>Cree aplicaciones 10 veces más rápido.</div>
                            <Link to='/register'>
                                <Button className='buttonA' style={{ width: '50%', height: '5vh', marginTop: '1.5vh', marginLeft: '0%', marginBottom: '1.5vh', opacity: '0.75', backgroundColor: 'linear-gradient(to right, #4B0082, #8B008B) important!', fontSize: '12.5px' }}>REGISTRATE</Button>
                            </Link>
                        </div>
                    </div>
                    <div style={{ display: 'flex', flexDirection: 'row', height: '50%' }}>
                        <FontAwesomeIcon icon={faBook} style={{ width: '44px', height: '44px', margin: '1vh 1vw 0vh 1vw' }} />
                        <div style={{ display: 'flex', flexDirection: 'column', width: '50%' }}>
                            <div >Books</div>
                            <div style={{ fontSize: '15px' }}>Potente plataforma financiera para empresas en crecimiento.</div>
                            <Link to='/register'>
                                <Button className='buttonA' style={{ width: '50%', height: '5vh', marginTop: '1.5vh', marginLeft: '0%', marginBottom: '1.5vh', opacity: '0.75', backgroundColor: 'linear-gradient(to right, #4B0082, #8B008B) important!', fontSize: '12.5px' }}>REGISTRATE</Button>
                            </Link>
                        </div>
                        <FontAwesomeIcon icon={faDesktop} style={{ width: '44px', height: '44px', margin: '1vh 1vw 0vh 1vw' }} />
                        <div style={{ display: 'flex', flexDirection: 'column', width: '50%' }}>
                            <div>Desk</div>
                            <div style={{ fontSize: '15px' }}>Servicio de asistencia contextual.</div>
                            <Link to='/register'>
                                <Button className='buttonA' style={{ width: '50%', height: '5vh', marginTop: '1.5vh', marginLeft: '0%', marginBottom: '1.5vh', opacity: '0.75', backgroundColor: 'linear-gradient(to right, #4B0082, #8B008B) important!', fontSize: '12.5px' }}>REGISTRATE</Button>
                            </Link>
                        </div>
                    </div>
                </div>
            </div>

            <p>TRABAJE REMOTAMENTE CON</p>
            <div className='xContent' style={{ display: 'flex', flexDirection: 'row' }}>
                <div style={{ display: 'flex', flexDirection: 'row' }}>
                    <div style={{ display: 'flex', flexDirection: 'column' }}>
                        <div>
                            {/* <FontAwesomeIcon icon={faUser} style={{ marginRight: '0vw' }} /> */}
                            <FontAwesomeIcon icon={faRecycle} style={{ width: '44px', height: '44px', margin: '1vh 1vw 0vh 1vw' }} />

                        </div>
                        <div>
                            Remotely
                        </div>
                    </div>
                    <div style={{ display: 'flex', flexDirection: 'column' }}>
                        <div style={{ fontSize: '24px', fontWeight: 'bold' }}>Su conjunto de herramientas para el trabajo desde casa</div>
                        <div style={{ height: '36%' }}>Un conjunto de aplicaciones móviles y web diseñado para equipos dispersos.</div>
                        <Link to='/register'>
                            <Button className='buttonA' style={{ width: '50%', marginLeft: '0%', marginTop: '5vh', marginBottom: '5vh' }}>REGISTRARSE AHORA</Button>
                        </Link>
                    </div>
                </div>
                <div style={{ display: 'flex', flexDirection: 'column' }}>
                    <div style={{ display: 'flex', flexDirection: 'row', height: '50%' }}>
                        <FontAwesomeIcon icon={faHandsHelping} style={{ width: '44px', height: '44px', margin: '1vh 1vw 0vh 1vw' }} />
                        <div style={{ display: 'flex', flexDirection: 'column', width: '50%' }}>
                            <div style={{ fontSize: '18px', fontWeight: 'bold' }}>Assist</div>
                            <div style={{ fontSize: '15px' }}>Solución de soporte remoto y acceso desatendido</div>
                            <Link to='/register'>
                                <Button className='buttonA' style={{ width: '50%', height: '5vh', marginTop: '1.5vh', marginLeft: '0%', marginBottom: '1.5vh', opacity: '0.75', backgroundColor: 'linear-gradient(to right, #4B0082, #8B008B) important!', fontSize: '12.5px' }}>REGISTRATE</Button>
                            </Link>
                        </div>
                        <FontAwesomeIcon icon={faPeopleArrows} style={{ width: '44px', height: '44px', margin: '1vh 1vw 0vh 1vw' }} />
                        <div style={{ display: 'flex', flexDirection: 'column', width: '50%' }}>
                            <div style={{ fontSize: '18px', fontWeight: 'bold' }}>Meeting</div>
                            <div style={{ fontSize: '15px' }}>Solución de reuniones y seminarios web.</div>
                            <Link to='/register'>
                                <Button className='buttonA' style={{ width: '50%', height: '5vh', marginTop: '1.5vh', marginLeft: '0%', marginBottom: '1.5vh', opacity: '0.75', backgroundColor: 'linear-gradient(to right, #4B0082, #8B008B) important!', fontSize: '12.5px' }}>REGISTRATE</Button>
                            </Link>
                        </div>
                    </div>
                    <div style={{ display: 'flex', flexDirection: 'row', height: '50%' }}>
                        <FontAwesomeIcon icon={faRunning} style={{ width: '44px', height: '44px', margin: '1vh 1vw 0vh 1vw' }} />
                        <div style={{ display: 'flex', flexDirection: 'column', width: '50%' }}>
                            <div >Sprints</div>
                            <div style={{ fontSize: '15px' }}>Herramienta de planificación y seguimiento para equipos ágiles.</div>
                            <Link to='/register'>
                                <Button className='buttonA' style={{ width: '50%', height: '5vh', marginTop: '1.5vh', marginLeft: '0%', marginBottom: '1.5vh', opacity: '0.75', backgroundColor: 'linear-gradient(to right, #4B0082, #8B008B) important!', fontSize: '12.5px' }}>REGISTRATE</Button>
                            </Link>
                        </div>
                        <FontAwesomeIcon icon={faClipboardCheck} style={{ width: '44px', height: '44px', margin: '1vh 1vw 0vh 1vw' }} />
                        <div style={{ display: 'flex', flexDirection: 'column', width: '50%' }}>
                            <div>Cliq</div>
                            <div style={{ fontSize: '15px' }}>Chat diseñado para el trabajo.</div>
                            <Link to='/register'>
                                <Button className='buttonA' style={{ width: '50%', height: '5vh', marginTop: '3.5vh', marginLeft: '0%', marginBottom: '1.5vh', opacity: '0.75', backgroundColor: 'linear-gradient(to right, #4B0082, #8B008B) important!', fontSize: '12.5px' }}>REGISTRATE</Button>
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
            <p>PAQUETES</p>
            <div className='xContent' style={{ display: 'flex', flexDirection: 'row' }}>
                <div style={{ display: 'flex', flexDirection: 'column' }}>
                    <FontAwesomeIcon icon={faCube} style={{ width: '60px', height: '60px', margin: '1vh 1vw 0vh 1vw' }} />
                    <div >Zoho One</div>
                </div>
                <div style={{ display: 'flex', flexDirection: 'column' }}>
                    <p>Administre toda su empresa con más de 45 aplicaciones integradas. Con Zoho One, podrá gestionar, conectar y automatizar los procesos empresariales de toda su organización. Experimente el sistema operativo para las empresas.</p>
                    <Link to='/aboutUs'>
                        <Button className='buttonA' style={{ width: '15vw', marginLeft: '0%', marginBottom: '3vh' }}>Más información</Button>
                    </Link>
                </div>
            </div>
            <div>OPTIONS</div>
            <div className='xContent' style={{ display: 'flex', flexDirection: 'row' }}>
                <div style={{ display: 'flex', flexDirection: 'column', width: '33%' }}>
                    <div style={{ display: 'flex', flexDirection: 'row', height: '50%' }}>

                        <FontAwesomeIcon icon={faCube} style={{ width: '44px', height: '44px', margin: '1vh 1vw 0vh 1vw', color: 'blue' }} />
                        <div style={{ display: 'flex', flexDirection: 'column' }}>
                            <div style={{ fontSize: '18px', fontWeight: 'bold' }}>CRM Plus</div>
                            <div style={{ fontSize: '15px' }}>Plataforma para una experiencia del cliente unificada.</div>
                            <Link to='/register'>
                                <Button className='buttonA' style={{ width: '50%', height: '5vh', marginTop: '1.5vh', marginLeft: '0%', marginBottom: '1.5vh', opacity: '0.75', backgroundColor: 'linear-gradient(to right, #4B0082, #8B008B) important!', fontSize: '12.5px' }}>REGISTRATE</Button>
                            </Link>
                        </div>
                    </div>
                    <div style={{ display: 'flex', flexDirection: 'row', height: '50%' }}>

                        <FontAwesomeIcon icon={faCube} style={{ width: '44px', height: '44px', margin: '1vh 1vw 0vh 1vw', color: 'yellow' }} />
                        <div style={{ display: 'flex', flexDirection: 'column' }}>
                            <div>Creator Plus</div>
                            <div style={{ fontSize: '15px' }}>Plataforma para la automatización y la transformación digital.</div>
                            <Link to='/register'>
                                <Button className='buttonA' style={{ width: '50%', height: '5vh', marginTop: '1.5vh', marginLeft: '0%', marginBottom: '1.5vh', opacity: '0.75', backgroundColor: 'linear-gradient(to right, #4B0082, #8B008B) important!', fontSize: '12.5px' }}>REGISTRATE</Button>
                            </Link>
                        </div>
                    </div>
                </div>
                <div style={{ display: 'flex', flexDirection: 'column', width: '67%' }}>
                    <div style={{ display: 'flex', flexDirection: 'row', height: '50%' }}>
                        <FontAwesomeIcon icon={faCube} style={{ width: '44px', height: '44px', margin: '1vh 1vw 0vh 1vw', color: 'red' }} />
                        <div style={{ display: 'flex', flexDirection: 'column', width: '50%' }}>
                            <div style={{ fontSize: '18px', fontWeight: 'bold' }}>Workplace</div>
                            <div style={{ fontSize: '15px' }}>Todas las herramientas para trabajar en un conjunto integrado.</div>
                            <Link to='/register'>
                                <Button className='buttonA' style={{ width: '50%', height: '5vh', marginTop: '1.5vh', marginLeft: '0%', marginBottom: '1.5vh', opacity: '0.75', backgroundColor: 'linear-gradient(to right, #4B0082, #8B008B) important!', fontSize: '12.5px' }}>REGISTRATE</Button>
                            </Link>
                        </div>
                        <FontAwesomeIcon icon={faCube} style={{ width: '44px', height: '44px', margin: '1vh 1vw 0vh 1vw', color: '#4B0082' }} />
                        <div style={{ display: 'flex', flexDirection: 'column', width: '50%' }}>
                            <div style={{ fontSize: '18px', fontWeight: 'bold' }}>People Plus</div>
                            <div style={{ fontSize: '15px' }}>Plataforma unificada de RR. HH.</div>
                            <Link to='/register'>
                                <Button className='buttonA' style={{ width: '50%', height: '5vh', marginTop: '4vh', marginLeft: '0%', marginBottom: '1.5vh', opacity: '0.75', backgroundColor: 'linear-gradient(to right, #4B0082, #8B008B) important!', fontSize: '12.5px' }}>REGISTRATE</Button>
                            </Link>
                        </div>
                    </div>
                    <div style={{ display: 'flex', flexDirection: 'row', height: '50%' }}>
                        <FontAwesomeIcon icon={faCube} style={{ width: '44px', height: '44px', margin: '1vh 1vw 0vh 1vw', color: 'black' }} />
                        <div style={{ display: 'flex', flexDirection: 'column', width: '50%' }}>
                            <div >Finance Plus</div>
                            <div style={{ fontSize: '15px' }}>Plataforma unificada de finanzas para empresas.</div>
                            <Link to='/register'>
                                <Button className='buttonA' style={{ width: '50%', height: '5vh', marginTop: '1.5vh', marginLeft: '0%', marginBottom: '1.5vh', opacity: '0.75', backgroundColor: 'linear-gradient(to right, #4B0082, #8B008B) important!', fontSize: '12.5px' }}>REGISTRATE</Button>
                            </Link>
                        </div>
                        <FontAwesomeIcon icon={faCube} style={{ width: '44px', height: '44px', margin: '1vh 1vw 0vh 1vw', color: '#D2691E' }} />
                        <div style={{ display: 'flex', flexDirection: 'column', width: '50%' }}>
                            <div>IT Management</div>
                            <div style={{ fontSize: '15px' }}>Lo ayudamos a alinear la TI con los negocios.</div>
                            <Link to='/register'>
                                <Button className='buttonA' style={{ width: '50%', height: '5vh', marginTop: '4vh', marginLeft: '0%', marginBottom: '1.5vh', opacity: '0.75', backgroundColor: 'linear-gradient(to right, #4B0082, #8B008B) important!', fontSize: '12.5px' }}>REGISTRATE</Button>
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
            <div>TODAS LAS APLICACIONES DE ZOHO</div>
            <div className='xContent' style={{ display: 'flex', flexDirection: 'row' }}>
                <div style={{ display: 'flex', flexDirection: 'column', width: '33%' }}>
                    Ventas y mercadeo
                    <p>Bríndele a su equipo de ventas el conjunto perfecto de aplicaciones para ayudarlo a cerrar más acuerdos en menos tiempo.</p>
                    <div style={{ display: 'flex', flexDirection: 'row' }}>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '33%' }}>
                            <Link to='/register'>
                                <FaInfinity size='44px' style={{ margin: '1vh 1vw 0vh 1vw', color: 'white' }} />
                            </Link>
                            <div>CRM</div>
                        </div>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '33%' }}>
                            <Link to='/register'>
                                <FontAwesomeIcon icon={faBacon} style={{ width: '44px', height: '44px', margin: '1vh 1vw 0vh 1vw', color: 'white' }} />
                            </Link>
                            <div>BIGIN</div>
                        </div>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '33%' }}>
                            <Link to='/register'>
                                <FontAwesomeIcon icon={faCalendar} style={{ width: '44px', height: '44px', margin: '1vh 1vw 0vh 1vw', color: 'white' }} />
                            </Link>
                            <div>CAMPAIGNS</div>
                        </div>
                    </div>
                    <div style={{ display: 'flex', flexDirection: 'row' }}>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '33%' }}>
                            <Link to='/register'>
                                <FontAwesomeIcon icon={faFolder} style={{ width: '44px', height: '44px', margin: '1vh 1vw 0vh 1vw', color: 'white' }} />
                            </Link>
                            <div>FORMS</div>
                        </div>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '33%' }}>
                            <Link to='/register'>
                                <FontAwesomeIcon icon={faSign} style={{ width: '44px', height: '44px', margin: '1vh 1vw 0vh 1vw', color: 'white' }} />
                            </Link>
                            <div>SIGN</div>
                        </div>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '33%' }}>
                            <Link to='/register'>
                                <FontAwesomeIcon icon={faSchool} style={{ width: '44px', height: '44px', margin: '1vh 1vw 0vh 1vw', color: 'white' }} />
                            </Link>
                            <div>SOCIAL</div>
                        </div>
                    </div>
                    <div style={{ display: 'flex', flexDirection: 'row' }}>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '33%' }}>
                            <Link to='/register'>
                                <FontAwesomeIcon icon={faPersonBooth} style={{ width: '44px', height: '44px', margin: '1vh 1vw 0vh 1vw', color: 'white' }} />
                            </Link>
                            <div>SALESIQ</div>
                        </div>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '33%' }}>
                            <Link to='/register'>
                                <FontAwesomeIcon icon={faSurprise} style={{ width: '44px', height: '44px', margin: '1vh 1vw 0vh 1vw', color: 'white' }} />
                            </Link>
                            <div>SURVEY</div>
                        </div>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '33%' }}>
                            <Link to='/register'>
                                <FontAwesomeIcon icon={faProcedures} style={{ width: '44px', height: '44px', margin: '1vh 1vw 0vh 1vw', color: 'white' }} />
                            </Link>
                            <div>SALESINBOX</div>
                        </div>
                    </div>
                    <div style={{ display: 'flex', flexDirection: 'row' }}>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '33%' }}>
                            <Link to='/register'>
                                <FontAwesomeIcon icon={faSatellite} style={{ width: '44px', height: '44px', margin: '1vh 1vw 0vh 1vw', color: 'white' }} />
                            </Link>
                            <div>SITES</div>
                        </div>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '33%' }}>
                            <Link to='/register'>
                                <FontAwesomeIcon icon={faNewspaper} style={{ width: '44px', height: '44px', margin: '1vh 1vw 0vh 1vw', color: 'white' }} />
                            </Link>
                            <div>PAGESENSE</div>
                        </div>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '33%' }}>
                            <Link to='/register'>
                                <FontAwesomeIcon icon={faMapSigns} style={{ width: '44px', height: '44px', margin: '1vh 1vw 0vh 2.5vw', color: 'white' }} />

                            </Link>
                            <div>BACKSTAGE</div>
                        </div>
                    </div>
                    <div style={{ display: 'flex', flexDirection: 'row' }}>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '33%' }}>
                            <Link to='/register'>
                                <FontAwesomeIcon icon={faGift} style={{ width: '44px', height: '44px', margin: '1vh 1vw 0vh 1vw', color: 'white' }} />
                            </Link>
                            <div>COMMERCE</div>
                        </div>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '33%' }}>
                            <Link to='/register'>
                                <FontAwesomeIcon icon={faReceipt} style={{ width: '44px', height: '44px', margin: '1vh 1vw 0vh 1vw', color: 'white' }} />
                            </Link>
                            <div>BOOKINGS</div>
                        </div>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '33%' }}>
                            <Link to='/register'>
                                <FontAwesomeIcon icon={faReply} style={{ width: '44px', height: '44px', margin: '1vh 1vw 0vh 1vw', color: 'white' }} />
                            </Link>
                            <div>MARKETINGHUB</div>
                        </div>
                    </div>
                </div>
                <div style={{ display: 'flex', flexDirection: 'column', width: '33%' }}>
                    Correo electrónico y colaboración
                    <p>
                        Capacite a su fuerza de trabajo con aplicaciones de colaboración y transforme la forma en que trabajan.
                    </p>
                    <div style={{ display: 'flex', flexDirection: 'row' }}>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '33%' }}>
                            <Link to='/register'>
                                <FaMailBulk size='44px' style={{ margin: '1vh 1vw 0vh 1vw', color: 'white' }} />
                            </Link>
                            <div>MAIL</div>
                        </div>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '33%' }}>
                            <Link to='/register'>
                                <FontAwesomeIcon icon={faClipboardCheck} style={{ width: '44px', height: '44px', margin: '1vh 1vw 0vh 1vw', color: 'white' }} />
                            </Link>
                            <div>CLIQ</div>
                        </div>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '33%' }}>
                            <Link to='/register'>
                                <FontAwesomeIcon icon={faWalking} style={{ width: '44px', height: '44px', margin: '1vh 1vw 0vh 1vw', color: 'white' }} />
                            </Link>
                            <div>WORKDRIVE</div>
                        </div>
                    </div>
                    <div style={{ display: 'flex', flexDirection: 'row' }}>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '33%' }}>
                            <Link to='/register'>
                                <FontAwesomeIcon icon={faFolderPlus} style={{ width: '44px', height: '44px', margin: '1vh 1vw 0vh 1vw', color: 'white' }} />
                            </Link>
                            <div>DOCS</div>
                        </div>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '33%' }}>
                            <Link to='/register'>
                                <FontAwesomeIcon icon={faPen} style={{ width: '44px', height: '44px', margin: '1vh 1vw 0vh 1vw', color: 'white' }} />
                            </Link>
                            <div>WRITER</div>
                        </div>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '33%' }}>
                            <Link to='/register'>
                                <FontAwesomeIcon icon={faShapes} style={{ width: '44px', height: '44px', margin: '1vh 1vw 0vh 1vw', color: 'white' }} />
                            </Link>
                            <div>SHEET</div>
                        </div>
                    </div>
                    <div style={{ display: 'flex', flexDirection: 'row' }}>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '33%' }}>
                            <Link to='/register'>
                                <FontAwesomeIcon icon={faVideo} style={{ width: '44px', height: '44px', margin: '1vh 1vw 0vh 1vw', color: 'white' }} />
                            </Link>
                            <div>SHOW</div>
                        </div>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '33%' }}>
                            <Link to='/register'>
                                <FontAwesomeIcon icon={faPaintBrush} style={{ width: '44px', height: '44px', margin: '1vh 1vw 0vh 1vw', color: 'white' }} />
                            </Link>
                            <div>PROJECTS</div>
                        </div>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '33%' }}>
                            <Link to='/register'>
                                <FontAwesomeIcon icon={faRunning} style={{ width: '44px', height: '44px', margin: '1vh 1vw 0vh 1vw', color: 'white' }} />
                            </Link>
                            <div>SPRINTS</div>
                        </div>
                    </div>
                    <div style={{ display: 'flex', flexDirection: 'row' }}>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '33%' }}>
                            <Link to='/register'>
                                <FontAwesomeIcon icon={faBug} style={{ width: '44px', height: '44px', margin: '1vh 1vw 0vh 1vw', color: 'white' }} />
                            </Link>
                            <div>BUGTRACKER</div>
                        </div>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '33%' }}>
                            <Link to='/register'>
                                <FontAwesomeIcon icon={faRecycle} style={{ width: '44px', height: '44px', margin: '1vh 1vw 0vh 1vw', color: 'white' }} />
                            </Link>
                            <div>CONNECT</div>
                        </div>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '33%' }}>
                            <Link to='/register'>
                                <FontAwesomeIcon icon={faPeopleArrows} style={{ width: '44px', height: '44px', margin: '1vh 1vw 0vh 1vw', color: 'white' }} />
                            </Link>
                            <div>MEETING</div>
                        </div>
                    </div>
                    <div style={{ display: 'flex', flexDirection: 'row' }}>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '33%' }}>
                            <Link to='/register'>
                                <FontAwesomeIcon icon={faBoxOpen} style={{ width: '44px', height: '44px', margin: '1vh 1vw 0vh 1vw', color: 'white' }} />
                            </Link>
                            <div>VAULT</div>
                        </div>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '33%' }}>
                            <Link to='/register'>
                                <FontAwesomeIcon icon={faVideo} style={{ width: '44px', height: '44px', margin: '1vh 1vw 0vh 1vw', color: 'white' }} />
                            </Link>
                            <div>SHOWTIME</div>
                        </div>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '33%' }}>
                            <Link to='/register'>
                                <FontAwesomeIcon icon={faNotesMedical} style={{ width: '44px', height: '44px', margin: '1vh 1vw 0vh 1vw', color: 'white' }} />
                            </Link>
                            <div>NOTEBOOK</div>
                        </div>
                    </div>
                    <div style={{ display: 'flex', flexDirection: 'row' }}>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '33%' }}>
                            <Link to='/register'>
                                <FontAwesomeIcon icon={faDatabase} style={{ width: '44px', height: '44px', margin: '1vh 1vw 0vh 1vw', color: 'white' }} />
                            </Link>
                            <div>WIKI</div>
                        </div>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '33%' }}>
                            <Link to='/register'>
                                <FontAwesomeIcon icon={faUserNinja} style={{ width: '44px', height: '44px', margin: '1vh 1vw 0vh 1vw', color: 'white' }} />
                            </Link>
                            <div>TEAMINBOX</div>
                        </div>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '33%' }}>
                            <Link to='/register'>
                                <FontAwesomeIcon icon={faJedi} style={{ width: '44px', height: '44px', margin: '1vh 1vw 0vh 1vw', color: 'white' }} />
                            </Link>
                            <div>TRANSMAIL</div>
                        </div>
                    </div>
                </div>
                <div style={{ display: 'flex', flexDirection: 'column', width: '34%' }}>
                    Finanzas
                    <p>
                        Resuelva los problemas de contabilidad empresarial con nuestro conjunto perfecto de aplicaciones de finanzas en la nube.
                    </p>
                    <div style={{ display: 'flex', flexDirection: 'row' }}>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '33%' }}>
                            <Link to='/register'>
                                <FontAwesomeIcon icon={faBook} style={{ width: '44px', height: '44px', margin: '1vh 1vw 0vh 1vw', color: 'white' }} />
                            </Link>
                            <div>BOOKS</div>
                        </div>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '33%' }}>
                            <Link to='/register'>
                                <FontAwesomeIcon icon={faStore} style={{ width: '44px', height: '44px', margin: '1vh 1vw 0vh 1vw', color: 'white' }} />
                            </Link>
                            <div>INVENTORY</div>
                        </div>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '33%' }}>
                            <Link to='/register'>
                                <FontAwesomeIcon icon={faFileInvoice} style={{ width: '44px', height: '44px', margin: '1vh 1vw 0vh 1vw', color: 'white' }} />
                            </Link>
                            <div>INVOICE</div>
                        </div>
                    </div>
                    <div style={{ display: 'flex', flexDirection: 'row' }}>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '33%' }}>
                            <Link to='/register'>
                                <FontAwesomeIcon icon={faSubscript} style={{ width: '44px', height: '44px', margin: '1vh 1vw 0vh 1vw', color: 'white' }} />
                            </Link>
                            <div>SUBSCRIPTIONS</div>
                        </div>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '33%' }}>
                            <Link to='/register'>
                                <FontAwesomeIcon icon={faMoneyBill} style={{ width: '44px', height: '44px', margin: '1vh 1vw 0vh 1vw', color: 'white' }} />
                            </Link>
                            <div>EXPENSE</div>
                        </div>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '33%' }}>
                            <Link to='/register'>
                                <FontAwesomeIcon icon={faCheck} style={{ width: '44px', height: '44px', margin: '1vh 1vw 0vh 1vw', color: 'white' }} />
                            </Link>
                            <div>CHECKOUT</div>
                        </div>
                    </div>
                    Recursos Humanos
                    <p>Concéntrese en sus empleados mientras nuestras aplicaciones permiten automatizar los procesos de recursos humanos.</p>
                    <div style={{ display: 'flex', flexDirection: 'row' }}>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '50%' }}>
                            <Link to='/register'>
                                <FontAwesomeIcon icon={faUserCheck} style={{ width: '44px', height: '44px', margin: '1vh 1vw 0vh 1vw', color: 'white' }} />
                            </Link>
                            <div>RECRUIT</div>
                        </div>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '50%' }}>
                            <Link to='/register'>
                                <FontAwesomeIcon icon={faPeopleCarry} style={{ width: '44px', height: '44px', margin: '1vh 1vw 0vh 1vw', color: 'white' }} />
                            </Link>
                            <div>PEOPLE</div>
                        </div>
                    </div>
                    <div style={{ display: 'flex', flexDirection: 'row' }}>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '50%' }}>
                            <Link to='/register'>
                                <FontAwesomeIcon icon={faHammer} style={{ width: '44px', height: '44px', margin: '1vh 1vw 0vh 1vw', color: 'white' }} />
                            </Link>
                            <div>WORKELY</div>
                        </div>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '50%' }}>
                            <Link to='/register'>
                                <FontAwesomeIcon icon={faCommentDots} style={{ width: '44px', height: '44px', margin: '1vh 1vw 0vh 1vw', color: 'white' }} />
                            </Link>
                            <div>BACKTOWORK</div>
                        </div>
                    </div>
                </div>
            </div>
            <div className='xContent' style={{ display: 'flex', flexDirection: 'row' }}>
                <div style={{ display: 'flex', flexDirection: 'column', width: '33%' }}>
                    TI y servicio de asistencia
                    <p>Esté justo donde están sus clientes gracias a aplicaciones que le permiten a su empresa interactuar con ellos.</p>
                    <div style={{ display: 'flex', flexDirection: 'row' }}>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '33%' }}>
                            <Link to='/register'>
                                <FontAwesomeIcon icon={faCode} style={{ width: '44px', height: '44px', margin: '1vh 1vw 0vh 1vw', color: 'white' }} />
                            </Link>
                            <div>CATALYST</div>
                        </div>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '33%' }}>
                            <Link to='/register'>
                                <FontAwesomeIcon icon={faUserFriends} style={{ width: '44px', height: '44px', margin: '1vh 1vw 0vh 1vw', color: 'white' }} />
                            </Link>
                            <div>SERVICEDESKPLUS</div>
                        </div>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '33%' }}>
                            <Link to='/register'>
                                <FontAwesomeIcon icon={faMobile} style={{ width: '44px', height: '44px', margin: '1vh 1vw 0vh 1vw', color: 'white' }} />
                            </Link>
                            <div>MOBILE DEVICE MANAGEMENT</div>
                        </div>
                    </div>
                    <div style={{ display: 'flex', flexDirection: 'row' }}>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '33%' }}>
                            <Link to='/register'>
                                <FontAwesomeIcon icon={faMagic} style={{ width: '44px', height: '44px', margin: '1vh 1vw 0vh 1vw', color: 'white' }} />
                            </Link>
                            <div>PATCH MANAGER PLUS</div>
                        </div>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '33%' }}>
                            <Link to='/register'>
                                <FontAwesomeIcon icon={faLaptop} style={{ width: '44px', height: '44px', margin: '1vh 1vw 0vh 1vw', color: 'white' }} />
                            </Link>
                            <div>REMOTE ACCESS PLUS</div>
                        </div>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '33%' }}>
                            <Link to='/register'>
                                <FontAwesomeIcon icon={faDownload} style={{ width: '44px', height: '44px', margin: '1vh 1vw 0vh 1vw', color: 'white' }} />
                            </Link>
                            <div>SITE24X7</div>
                        </div>
                    </div>
                    <div style={{ display: 'flex', flexDirection: 'row' }}>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '33%' }}>
                            <Link to='/register'>
                                <FontAwesomeIcon icon={faCloud} style={{ width: '44px', height: '44px', margin: '1vh 1vw 0vh 1vw', color: 'white' }} />
                            </Link>
                            <div>LOG MANAGEMENT CLOUD</div>
                        </div>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '33%' }}>
                            <Link to='/register'>
                                <FontAwesomeIcon icon={faPaw} style={{ width: '44px', height: '44px', margin: '1vh 1vw 0vh 1vw', color: 'white' }} />
                            </Link>
                            <div>IDENTITY MANAGEMENT</div>
                        </div>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '33%' }}>
                            <Link to='/register'>
                                <FontAwesomeIcon icon={faHatWizard} style={{ width: '44px', height: '44px', margin: '1vh 1vw 0vh 1vw', color: 'white' }} />
                            </Link>
                            <div>DESKTOP CENTRAL</div>
                        </div>
                    </div>
                </div>
                <div style={{ display: 'flex', flexDirection: 'column', width: '33%' }}>
                    Servicio al cliente
                    <p>
                        Proporcione a su equipo de servicios las herramientas y el contexto adecuados y necesarios para que cada cliente tenga éxito.
                    </p>
                    <div style={{ display: 'flex', flexDirection: 'row' }}>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '33%' }}>
                            <Link to='/register'>
                                <FontAwesomeIcon icon={faDesktop} style={{ width: '44px', height: '44px', margin: '1vh 1vw 0vh 1vw', color: 'white' }} />
                            </Link>
                            <div>DESK</div>
                        </div>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '33%' }}>
                            <Link to='/register'>
                                <FontAwesomeIcon icon={faHandsHelping} style={{ width: '44px', height: '44px', margin: '1vh 1vw 0vh 1vw', color: 'white' }} />
                            </Link>
                            <div>ASSIST</div>
                        </div>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '33%' }}>
                            <Link to='/register'>
                                <FontAwesomeIcon icon={faGlasses} style={{ width: '44px', height: '44px', margin: '1vh 1vw 0vh 1vw', color: 'white' }} />
                            </Link>
                            <div>LENS</div>
                        </div>
                    </div>

                    Business Intelligence
                    <p>Potencie su negocio con información detallada de sus datos gracias a la aplicación de inteligencia empresarial y de análisis.</p>
                    <div style={{ display: 'flex', flexDirection: 'row' }}>
                        <div style={{ display: 'flex', flexDirection: 'column', marginLeft: '33%', width: '33%' }}>
                            <Link to='/register'>
                                <FontAwesomeIcon icon={faChartBar} style={{ width: '44px', height: '44px', margin: '1vh 1vw 0vh 1vw', color: 'white' }} />
                            </Link>
                            <div>ANALYTICS</div>
                        </div>
                    </div>
                </div>
                <div style={{ display: 'flex', flexDirection: 'column', width: '34%' }}>

                    Soluciones personalizadas
                    <p>Simplifique los procesos empresariales complejos con aplicaciones que facilitarán el trabajo de su equipo.</p>
                    <div style={{ display: 'flex', flexDirection: 'row' }}>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '33%' }}>
                            <Link to='/register'>
                                <FontAwesomeIcon icon={faCube} style={{ width: '44px', height: '44px', margin: '1vh 1vw 0vh 1vw', color: 'white' }} />
                            </Link>
                            <div>CREATOR</div>
                        </div>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '33%' }}>
                            <Link to='/register'>
                                <FontAwesomeIcon icon={faStarOfLife} style={{ width: '44px', height: '44px', margin: '1vh 1vw 0vh 1vw', color: 'white' }} />
                            </Link>
                            <div>FLOW</div>
                        </div>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '33%' }}>
                            <Link to='/register'>
                                <FontAwesomeIcon icon={faStickyNote} style={{ width: '44px', height: '44px', margin: '1vh 1vw 0vh 1vw', color: 'white' }} />
                            </Link>
                            <div>INTEGRATOR</div>
                        </div>
                    </div>

                </div>
            </div>
            <div>RECURSOS ADICIONALES</div>
            <div className='xContent' style={{ display: 'flex', flexDirection: 'row' }}>
                <div style={{ display: 'flex', flexDirection: 'column', width: '25%' }}>
                    <div style={{ display: 'flex', flexDirection: 'row' }}>
                        <FontAwesomeIcon icon={faMedkit} style={{ width: '20px', height: '20px', margin: '0.1vh 1vw 0vh 2.5vw' }} />
                        <div>Recursos del COVID-19</div>
                    </div>
                    <p>Iniciativas de alivio y recursos de trabajo remoto para ayudarlo a superar tiempos difíciles.</p>
                    <Link to='/aboutUs'>
                        <Button className='buttonA' style={{ width: '40%', marginLeft: '0%' }}>Más Información</Button>
                    </Link>
                </div>
                <div style={{ display: 'flex', flexDirection: 'column', width: '25%' }}>
                    <div style={{ display: 'flex', flexDirection: 'row' }}>
                        <FontAwesomeIcon icon={faPeopleCarry} style={{ width: '20px', height: '20px', margin: '0.1vh 1vw 0vh 2.5vw' }} />
                        <div>Asociación con Zoho</div>
                    </div>
                    <p>Zoho se asocia con VAR, MSP, SI, consultores y socios tecnológicos de primer nivel.</p>
                    <Link to='/aboutUs'>
                        <Button className='buttonA' style={{ width: '40%', marginLeft: '0%' }}>Más Información</Button>
                    </Link>
                </div>
                <div style={{ display: 'flex', flexDirection: 'column', width: '25%' }}>
                    <div style={{ display: 'flex', flexDirection: 'row' }}>
                        <FontAwesomeIcon icon={faProjectDiagram} style={{ width: '20px', height: '20px', margin: '0.1vh 1vw 0vh 2.5vw' }} />
                        <div>Developer</div>
                    </div>
                    <p>Diseñe y venda extensiones para productos Zoho.</p>
                    <Link to='/aboutUs'>
                        <Button className='buttonA' style={{ width: '40%', marginLeft: '0%', marginTop: '3.5vh', marginBottom: '3.5vh' }}>Más Información</Button>
                    </Link>
                </div>
                <div style={{ display: 'flex', flexDirection: 'column', width: '25%' }}>
                    <div style={{ display: 'flex', flexDirection: 'row' }}>
                        <FontAwesomeIcon icon={faGift} style={{ width: '20px', height: '20px', margin: '0.1vh 1vw 0vh 2.5vw' }} />
                        <div>Marketplace</div>
                    </div>
                    <p>Instale extensiones que permitan agregar nuevas funciones a los productos Zoho.</p>
                    <Link to='/aboutUs'>
                        <Button className='buttonA' style={{ width: '40%', marginLeft: '0%' }}>Más Información</Button>
                    </Link>
                </div>
            </div>
            <div>:D</div>
            <div className='xContentX' style={{ display: 'flex', flexDirection: 'column', width: '70%', marginLeft: '15%', marginBottom: '5vh', justifyContent: 'center' }}>
                La comunidad Zoho
                <p>Únase a la comunidad de usuarios de habla hispana y siga aprendiendo y poniendo en práctica sus conocimientos sobre aplicaciones Zoho.</p>
                <Link to='/aboutUs'>
                    <Button className='buttonA' style={{ width: '40%', marginLeft: '0%', marginBottom: '2vh' }}>Más Información</Button>
                </Link>
            </div>
            {/* <Button className='buttonA' onClick={myLastTry}>PlayMe</Button> */}
            {/* <div className='contentMenu'>
                <div class="content1">
                    <Link to='/register'>
                        <div className='imageController' />
                    </Link>
                    <div>SunnyPass, la mejor opción en el mercado</div>
                </div>

                <div class="content2">
                    <div className='imageCatalog' onClick={myLastTry} />
                    <div>Catálogo de juegos dentro de SunnyPass</div>
                </div>
                <div class="content3">
                    <Link to='/getApp'>
                        <div className='imageApp' />
                    </Link>
                    <div>Mejores Ofertas Ahora</div>
                </div>
            </div> */}
            {/* The lights are {isOn ? 'on' : 'off'} and you've
            clicked {count} time(s). */}
            {/* Has abierto o cerrado el catálogo {count} ve{(count > 1 || count === 0) ? 'ces' : 'z'}. */}
            <div></div>
            { isOn ? 'Galería Abierta' : ''}
            { isOnFunction()}



            {/* <div id="myDIV" style={{ display: 'none' }}>Hola que hace!!!</div> */}

            {/* <h3>
                <span class="fas fa-university" style={{ marginRight: '0.3vw' }}></span>
                En esta área se mostrará la información del producto a vender!!!!
            </h3> */}

        </div >
    );
}

export default Form;