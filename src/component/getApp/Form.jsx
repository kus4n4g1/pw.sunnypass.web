import React, { useState } from 'react';
//import headerBar from './headerBar';
import Footer from '../footer';
import TopBar from '../topBar';
import Content from '../content';
import SideBar from '../sideBar';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUser } from '@fortawesome/free-solid-svg-icons';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { Button } from 'reactstrap';
import { Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import ReactTable from 'react-table';
import * as ReactBootstrap from 'react-bootstrap';
import { Route, Redirect, useHistory } from "react-router-dom";









const Form = () => {

    let history = useHistory();


    // const handleSubmit = (e) => {
    //     e.preventDefault();
    //     value.setStudent(prevStudent => [...prevStudent, {
    //         name: name,
    //         lastName: lastName,
    //         lastName2: lastName2,
    //         email: email,
    //         birthday: birthday,
    //         semester: semester,
    //         gender: gender,
    //         club: club,
    //         curp: curp,
    //         bloodType: bloodType,
    //         allergies: allergies,
    //         diseases: diseases
    //     }]);
    //     //console.log(value.student)
    // }

    // const renderPlayer = (player, index) => {
    //     return (
    //         <tr key={player.id}  >
    //             <td> {player.nombre} </td>
    //             <td> {player.paterno} </td>
    //             <td> {player.materno} </td>
    //             <td> {player.curp} </td>
    //             <td> {player.email} </td>
    //             <td> {player.semestre} </td>
    //             <td style={{ display: 'flex' }} >
    //                 {/* <a href="#" class="btn btn-success btn-lg">
    //                     <span class="glyphicon glyphicon-remove"></span> Borrar
    //                 </a> */}

    //                 <a style={{ color: '#6A5ACD', marginRight: '0.5vw' }} onClick={() => getStudentX({ variables: { id: player.id } })}>
    //                     <span className="fas fa-eye"></span>
    //                     <ModalStudent
    //                         modal={modal}
    //                         setModal={setModal}
    //                         studentX={studentX}
    //                     />
    //                 </a>


    //                 <a style={{ color: '#9400D3', marginRight: '0.5vw' }} onClick={() => getStudentX({ variables: { id: player.id } })}>
    //                     <span className="fas fa-edit"></span>
    //                     <ModalStudentMod
    //                         modal={modal}
    //                         setModal={setModal}
    //                         studentX={studentX}
    //                     />
    //                 </a>
    //                 <a style={{ color: 'red', marginRight: '0vw', marginleft: '0.5vw' }} onClick={() => getStudent({ variables: { id: player.id } })


    //                 }>
    //                     <span className="fas fa-trash-alt"></span>

    //                 </a>



    //                 {/* <button type="button" class="buttonDelete">
    //                 </button> */}

    //             </td>
    //         </tr>
    //     )
    // }

    const headerReady = () => {

        return (
            <tr>
                <th colSpan='7' style={{ backgroundImage: 'linear-gradient(to right, #9400D3, #DA70D6)' }}>
                    <span class="fas fa-user-graduate" style={{ marginRight: '0.3vw' }}></span>
                    MEJORES OFERTAS DE SUNNYPASS</th>
            </tr>
        );

    }

    function swalAlert(e) {
        Swal.fire({
            background: 'linear-gradient(to right, 	#9370DB, white)',
            color: 'white',
            title: 'Muchas Gracias',
            text: "Compra exitosa!",
            icon: 'success',
            showCancelButton: true,
            confirmButtonColor: '#9400D3',
            cancelButtonColor: '#3085d6',
            confirmButtonText: 'Volver a Inicio',
            cancelButtonText: 'Seguir Comprando'
        }).then((result) => {
            if (result.value) {
                history.push('/');
                Swal.fire({
                    background: 'linear-gradient(to right, 	#9370DB, white)',
                    title: 'Correcto',
                    text: "Felicidades el juego ya es Tuyo!",
                    icon: 'info',
                }
                )
            } else {
                history.push('/getApp')
            }
        })
    }
    var columns = [
        { title: "Tipo", dataKey: "Tipo" },
        { title: "Nombre", dataKey: "Nombre" },
        { title: "Paterno", dataKey: "Paterno" },
        { title: "Materno", dataKey: "Materno" },
        { title: "Email", dataKey: "Email" },
        { title: "Semestre", dataKey: "Semestre" },
        { title: "Género", dataKey: "Género" },
        { title: "T.Sangre", dataKey: "Sangre" },
        { title: "Alergias", dataKey: "Alergias" },
        { title: "Padecimientos", dataKey: "Padecimientos" },
    ];
    return (
        <>
            <div className='main'>
                <TopBar />
                <SideBar />
                <div className='contentX'>
                    <div className='bg'></div>
                    <ReactBootstrap.Table striped bordered hover style={{ backgroundColor: 'white', marginBottom: '0vh' }}
                    // SubComponent={row => {
                    //     return (
                    //         <div style={{ 'border': '1px solid rgba(0, 0, 0, 0.05)' }}>
                    //             {row.row.summary}
                    //         </div>
                    //     )
                    // }}
                    // onExpandedChange={newExpanded => onExpandedChange(newExpanded)}


                    >
                        <thead style={{ fontWeight: '500px', font: 'Roboto', color: 'white', backgroundImage: 'linear-gradient(to right, #BA55D3, #DDA0DD)' }}>
                            {headerReady()}
                            <tr>
                                <th>Nombre</th>
                                <th>Compañía</th>
                                <th>Disponibilidad</th>
                                <th>Oferta</th>
                                <th>Inicio</th>
                                <th>Término</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            {/* {console.log('dataR que se va a mapear', dataR.buscarTaller)} */}
                            {/* {dataR.buscarTaller.map(renderPlayerT)} */}
                            <tr style={{ color: '#9400D3' }}>
                                <th>Castlevania</th>
                                <th>Konami</th>
                                <th>Todo el mes</th>
                                <th>120 pesos</th>
                                <th>Hoy</th>
                                <th>Fin de mes</th>
                                <Button className='buttonAX' onClick={e => (swalAlert(e))}>Compra Ahora</Button>
                            </tr>
                            <tr style={{ color: '#9400D3', backgroundColor: '#DCDCDC' }}>
                                <th>King of Fighters XIV</th>
                                <th>SNK</th>
                                <th>15 días</th>
                                <th>300 pesos</th>
                                <th>Hoy</th>
                                <th>Dos Semanas</th>
                                <Button className='buttonAX' onClick={e => (swalAlert(e))}>Compra Ahora</Button>
                            </tr>
                            <tr style={{ color: '#9400D3' }}>
                                <th>Metal Slug</th>
                                <th>SNK</th>
                                <th>Todo el mes</th>
                                <th>70 pesos</th>
                                <th>Hoy</th>
                                <th>Fin de mes</th>
                                <Button className='buttonAX' onClick={e => (swalAlert(e))}>Compra Ahora</Button>
                            </tr>
                            <tr style={{ color: '#9400D3', backgroundColor: '#DCDCDC' }}>
                                <th>Ninja Gaiden Black</th>
                                <th>Tecmo</th>
                                <th>15 días</th>
                                <th>200 pesos</th>
                                <th>Hoy</th>
                                <th>Dos Semanas</th>
                                <Button className='buttonAX' onClick={e => (swalAlert(e))}>Compra Ahora</Button>
                            </tr>
                            <tr style={{ color: '#9400D3' }}>
                                <th>Metroid Prime</th>
                                <th>Nintendo</th>
                                <th>Todo el mes</th>
                                <th>250 pesos</th>
                                <th>Hoy</th>
                                <th>Fin de mes</th>
                                <Button className='buttonAX' onClick={e => (swalAlert(e))}>Compra Ahora</Button>
                            </tr>
                            <tr style={{ color: '#9400D3', backgroundColor: '#DCDCDC' }}>
                                <th>Super Mario Maker</th>
                                <th>Nintendo</th>
                                <th>15 días</th>
                                <th>800 pesos</th>
                                <th>Hoy</th>
                                <th>Dos Semanas</th>
                                <Button className='buttonAX' onClick={e => (swalAlert(e))}>Compra Ahora</Button>
                            </tr>
                            <tr style={{ color: '#9400D3' }}>
                                <th>Little Big Planet</th>
                                <th>Sony</th>
                                <th>Todo el mes</th>
                                <th>280 pesos</th>
                                <th>Hoy</th>
                                <th>Fin de mes</th>
                                <Button className='buttonAX' onClick={e => (swalAlert(e))}>Compra Ahora</Button>
                            </tr>
                            <tr style={{ color: '#9400D3', backgroundColor: '#DCDCDC' }}>
                                <th>Rival School</th>
                                <th>Fighting</th>
                                <th>15 días</th>
                                <th>90 pesos</th>
                                <th>Hoy</th>
                                <th>Dos Semanas</th>
                                <Button className='buttonAX' onClick={e => (swalAlert(e))}>Compra Ahora</Button>
                            </tr>
                            <tr style={{ color: '#9400D3' }}>
                                <th>Zelda Breath of the Wild</th>
                                <th>Nintendo</th>
                                <th>Todo el mes</th>
                                <th>777 pesos</th>
                                <th>Hoy</th>
                                <th>Fin de mes</th>
                                <Button className='buttonAX' onClick={e => (swalAlert(e))}>Compra Ahora</Button>
                            </tr>
                            <tr style={{ color: '#9400D3', backgroundColor: '#DCDCDC' }}>
                                <th>Halo Reach</th>
                                <th>SNK</th>
                                <th>15 días</th>
                                <th>450 pesos</th>
                                <th>Hoy</th>
                                <th>Dos Semanas</th>
                                <Button className='buttonAX' onClick={e => (swalAlert(e))}>Compra Ahora</Button>
                            </tr>
                            <tr style={{ color: '#9400D3' }}>
                                <th>Street Fighter IIIrd Strike</th>
                                <th>Konami</th>
                                <th>Todo el mes</th>
                                <th>180 pesos</th>
                                <th>Hoy</th>
                                <th>Fin de mes</th>
                                <Button className='buttonAX' onClick={e => (swalAlert(e))}>Compra Ahora</Button>
                            </tr>
                            <tr style={{ color: '#9400D3', backgroundColor: '#DCDCDC' }}>
                                <th>Silent Hill</th>
                                <th>SNK</th>
                                <th>15 días</th>
                                <th>45 pesos</th>
                                <th>Hoy</th>
                                <th>Dos Semanas</th>
                                <th>
                                    <Button className='buttonAX' onClick={e => (swalAlert(e))}>Compra Ahora</Button>
                                </th>
                            </tr>
                        </tbody>
                    </ReactBootstrap.Table>

                    <div className='containerRow1X'>
                        <Link to='/'>
                            <Button className='buttonA'>VOLVER</Button>
                        </Link>
                        <ul style={{ fontFamily: 'Roboto', fontSize: '18px', marginTop: '-3vh' }} class="c">
                            <li>Aprovecha</li>
                            <li>las</li>
                            <li>Ofertas</li>
                        </ul>

                    </div>
                </div>
            </div>
            <Footer />
        </>
    );
}

export default Form;