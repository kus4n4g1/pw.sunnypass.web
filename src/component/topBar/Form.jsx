import React from 'react';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faGlobeAmericas, faUser } from '@fortawesome/free-solid-svg-icons';
import {
    faSearch,
    faBook,
    faDesktop,
    faCube,
    faRecycle,
    faHandsHelping,
    faPeopleArrows,
    faRunning,
    faClipboardCheck,
    faBacon,
    faCalendar,
    faFolder,
    faSign,
    faSchool,
    faPersonBooth,
    faSurprise,
    faProcedures,
    faSatellite,
    faNewspaper,
    faMapSigns,
    faBug,
    faReceipt,
    faReply,
    faWalking,
    faFolderPlus,
    faPen,
    faShapes,
    faVideo,
    faPaintBrush,
    faBoxOpen,
    faVideoSlash,
    faNotesMedical,
    faDatabase,
    faUserNinja,
    faJedi,
    faStore,
    faFileInvoice,
    faSubscript,
    faMoneyBill,
    faCheck,
    faUserCheck,
    faPeopleCarry,
    faCode,
    faCommentDots,
    faUserFriends,
    faMobile,
    faMagic,
    faLaptop,
    faDownload,
    faCloud,
    faPaw,
    faHatWizard,
    faHammer,
    faGlasses,
    faStarOfLife,
    faStickyNote,
    faFire,
    faGift,
    faChartBar,
    faMedkit,
    faProjectDiagram

} from '@fortawesome/free-solid-svg-icons';
import { Link } from 'react-router-dom';
import { Button, Input } from 'reactstrap';
import { Route, Redirect, useHistory } from "react-router-dom";
import Swal from 'sweetalert2';


const Form = () => {
    let history = useHistory();

    function swalAlert(e) {

        if (e.key === 'Enter') {
            Swal.fire({
                background: 'linear-gradient(to right, 	#9370DB, white)',
                color: 'white',
                title: 'Correcto',
                text: "A continuación, se te dirige a las ofertas!",
                icon: 'success',
                showCancelButton: false,
                confirmButtonColor: '#9400D3',
                confirmButtonText: 'Continuar',
            }).then((result) => {
                if (result.value) {
                    history.push('/getApp');
                    Swal.fire({
                        background: 'linear-gradient(to right, 	#9370DB, white)',
                        title: 'Correcto',
                        text: "Momentáneamente la busqueda es manual. Sitio en Construcción. Disculpe las molestias!",
                        icon: 'info',
                    }
                    )
                } else {
                    history.push('/register')
                }
            })
        }

    }

    function swalAlert2(e) {

        Swal.fire({
            background: 'linear-gradient(to right, 	#9370DB, white)',
            color: 'white',
            title: 'Correcto',
            text: "A continuación, se te dirige a las ofertas!",
            icon: 'success',
            showCancelButton: false,
            confirmButtonColor: '#9400D3',
            confirmButtonText: 'Continuar',
        }).then((result) => {
            if (result.value) {
                history.push('/getApp');
                Swal.fire({
                    background: 'linear-gradient(to right, 	#9370DB, white)',
                    title: 'Correcto',
                    text: "Momentáneamente la busqueda es manual. Sitio en Construcción. Disculpe las molestias!",
                    icon: 'info',
                }
                )
            } else {
                history.push('/register')
            }
        })


    }

    return (
        <div className='topBar'>

            <div className='topBarFlex'>
                <div className='imageZoho'></div>

                <FontAwesomeIcon icon={faPeopleCarry} style={{ marginRight: '0vw' }} />
                <i class="fas fa-user"></i>
                <Link to='/login'>
                    <Button className='buttonA' style={{ fontSize: '16px', marginTop: '-1vh', height: '6vh', marginRight: '0.1vw', marginLeft: '0vw' }}>
                        {/* <span class="fas fa-user" style={{ marginRight: '0.3vw' }}></span> */}
                Clientes
            </Button>
                </Link>
                <FontAwesomeIcon icon={faHandsHelping} style={{ marginRight: '0vw' }} />
                <i class="fas fa-user"></i>
                <Link to='/sugest'>
                    <Button className='buttonA' style={{ fontSize: '16px', marginTop: '-1vh', height: '6vh', marginRight: '0.1vw', marginLeft: '0vw' }}>
                        {/* <span class="fas fa-user" style={{ marginRight: '0.3vw' }}></span> */}
                Soporte
            </Button>
                </Link>
                <FontAwesomeIcon icon={faGift} style={{ marginRight: '0vw' }} />
                <i class="fas fa-user"></i>
                <Link to='/getApp'>
                    <Button className='buttonA' style={{ fontSize: '16px', marginTop: '0vh important!', height: '6vh', marginRight: '0.1vw', marginLeft: '0vw' }}>
                        {/* <span class="fas fa-user" style={{ marginRight: '0.3vw' }}></span> */}
                Ventas
            </Button>
                </Link>
                <FontAwesomeIcon icon={faUser} style={{ marginRight: '0vw' }} />
                <i class="fas fa-user"></i>
                <Link to='/login'>
                    <Button className='buttonA' style={{ fontSize: '16px', marginTop: '-1vh', height: '6vh', marginRight: '0.1vw', marginLeft: '0vw' }}>
                        {/* <span class="fas fa-user" style={{ marginRight: '0.3vw' }}></span> */}
                Iniciar Sesión
            </Button>
                </Link>
                <FontAwesomeIcon icon={faSign} style={{ marginRight: '0vw' }} />
                <i class="fas fa-user"></i>
                <Link to='/calculate'>
                    <Button className='buttonA' style={{ fontSize: '16px', marginTop: '-1vh', height: '6vh', marginRight: '0.1vw', marginLeft: '0vw' }}>
                        {/* <span class="fas fa-user" style={{ marginRight: '0.3vw' }}></span> */}
                Cálculo
            </Button>
                </Link>
                <FontAwesomeIcon icon={faGlobeAmericas} style={{ marginRight: '-0.3vw' }} />
                <i class="fas fa-user"></i>
                <Link to='/'>
                    <Button className='buttonA' style={{ fontSize: '16px', marginTop: '-1vh', height: '6vh', marginRight: '0.1vw', marginLeft: '0vw' }}>
                        {/* <span class="fas fa-user" style={{ marginRight: '0.3vw' }}></span> */}
                Español
            </Button>
                </Link>
                <FontAwesomeIcon icon={faSearch} style={{ fontSize: '16px', marginRight: '0vw', color: 'white', marginTop: '1vh' }} onClick={e => swalAlert2(e)} />
                <Input
                    placeholder='Búsqueda'
                    style={{ height: '3.5vh', marginRight: '1vw', borderRadius: '10%', borderColor: 'indigo' }}
                    className='forHover'
                    onSubmit={(e) => swalAlert(e)}
                    onKeyUp={(e) => swalAlert(e)}

                />
            </div>

        </div>
    );
}

export default Form;