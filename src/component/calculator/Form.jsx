import React, { useState } from 'react';
//import headerBar from './headerBar';
import Footer from '../footer';
import TopBar from '../topBar';
import SideBar from '../sideBar';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus, faMinus, faDivide, faSortNumericDown, faSortNumericUp, faEquals, faAsterisk, faBirthdayCake, faMagic, faCar } from '@fortawesome/free-solid-svg-icons';
import { Button } from 'reactstrap';
import { Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import { useHistory } from "react-router-dom";
import Calendar from './calendar';
import moment from 'moment';








const Form = () => {

    const [number1, setNumber1] = useState(0);
    const [number2, setNumber2] = useState(0);
    const [suma, setSuma] = useState(0);
    const [resta, setResta] = useState(0);
    const [multiplicacion, setMultiplicacion] = useState(0);
    const [division, setDivision] = useState(0);
    const [exponenciacion, setExponenciacion] = useState(0);
    const [residuo, setResiduo] = useState(0);
    const [incremento, setIncremento] = useState(0);
    const [decremento, setDecremento] = useState(0);
    const [carro, setCarro] = useState(null);
    const [arrayCarros, setArrayCarros] = useState([{}]);
    const [asignSuma, setAsignSuma] = useState(0);
    const [asignResta, setAsignResta] = useState(0);
    const [asignMult, setAsignMult] = useState(0);
    const [asignDiv, setAsignDiv] = useState(0);
    const [asignResiduo, setAsignResiduo] = useState(0);
    const [asignCorrD, setAsignCorrD] = useState(0);
    const [asignCorrI, setAsignCorrI] = useState(0);
    const [asignCorrI2, setAsignCorrI2] = useState(0);
    const [asignAnd, setAsignAnd] = useState(0);
    const [asignNot, setAsignNot] = useState(0);
    const [asignOr, setAsignOr] = useState(0);
    const [asignExp, setAsignExp] = useState(0);
    const [asignIgual, setAsignIgual] = useState(0);
    const [nombre, setNombre] = useState('');
    const [apellido, setApellido] = useState('');
    const [marca, setMarca] = useState('');
    const [modelo, setModelo] = useState('');
    const [color, setColor] = useState('');
    const [compañia, setCompañia] = useState('');
    const [birthday, setBirthday] = useState(null);
    const [birthD, setBirthD] = useState();
    const [argumento, setArgumento] = useState('');
    const [argumento2, setArgumento2] = useState('');

    let history = useHistory();
    var arr = [];

    const dateB = Date.now.toString();
    var d = new Date();
    console.log(dateB);
    moment.locale('es');
    var formattedDate = moment.locale;

    // console.log('Este es el número 1: ' + number1);
    // console.log("Este es el número 2: ", number2);
    // const handleSubmit = (e) => {
    //     e.preventDefault();
    //     value.setStudent(prevStudent => [...prevStudent, {
    //         name: name,
    //         lastName: lastName,
    //         lastName2: lastName2,
    //         email: email,
    //         birthday: birthday,
    //         semester: semester,
    //         gender: gender,
    //         club: club,
    //         curp: curp,
    //         bloodType: bloodType,
    //         allergies: allergies,
    //         diseases: diseases
    //     }]);
    //     //console.log(value.student)
    // }
    function swalAlert(e) {
        e.preventDefault();
        console.log('Marca de carro en swalAlert' + carro.marca);
        console.log('compañia de carro en swalAlert' + carro.compañia);
        console.log('modelo de carro en swalAlert' + carro.modelo);
        console.log('color de carro en swalAlert' + carro.color);
        console.log('birthday de carro en swalAlert' + birthday);
        console.log('nombre de carro en swalAlert' + carro.nombre);
        console.log('apellido de carro en swalAlert' + carro.apellido);
        arr.push(...arr, { carro: carro });

        console.log('Arreglo en 0 de var arr: ' + arr[0].nombre);

        // setArrayCarros(arrayCarros => [
        //     ...arrayCarros,
        //     {
        //         marca: carro.marca,
        //         compañia: carro.compañia,
        //         modelo: carro.modelo,
        //         color: carro.color,
        //         birthday: birthday,
        //         nombre: carro.nombre,
        //         apellido: carro.apellido
        //     }
        // ]);
        // setArrayCarros([
        //     ...arrayCarros, {
        //         marca: carro.marca,
        //         compañia: carro.compañia,
        //         modelo: carro.modelo,
        //         color: carro.color,
        //         birthday: birthday,
        //         nombre: carro.nombre,
        //         apellido: carro.apellido
        //     }
        // ]);
        setArrayCarros(prevArrayCarros => [...prevArrayCarros, {
            marca: carro.marca,
            compañia: carro.compañia,
            modelo: carro.modelo,
            color: carro.color,
            birthday: birthday,
            nombre: carro.nombre,
            apellido: carro.apellido
        }]);
        // setArrayCarros(arr);
        // setArrayCarros(...arrayCarros, carro);

        //     [{
        // "marca": carro.marca,
        // "compañia": carro.compañia,
        // "modelo": carro.modelo,
        // "color": carro.color,
        // "birthday": birthday,
        // "nombre": carro.nombre,
        // "apellido": carro.apellido
        // }]);
        alert('Arreglo de objeto del Carro');
        alert(JSON.stringify(arr, null, 4));
        Swal.fire({
            background: 'linear-gradient(to right, 	#9370DB, white)',
            color: 'white',
            title: `Listo!: \nNúmero1: ${number1}, Número2: ${number2}`,
            text: `Suma: ${suma}, Resta: ${resta}, Multiplicación: ${multiplicacion}, División: ${division},\n
                   Exponenciación: ${exponenciacion}, Residuo: ${residuo}, Incremento del número1: ${incremento}, Decremento del número1: ${decremento}.
                   `,
            icon: 'success',
            showCancelButton: true,
            confirmButtonColor: '#4B0082',
            confirmButtonText: 'Volver a Inicio',
            cancelButtonText: 'Seguir Calculando',
            cancelButtonColor: '#9400D3',
        }).then((result) => {
            if (result.value) {
                history.push('/');
                Swal.fire({
                    background: 'linear-gradient(to right, 	#9370DB, white)',
                    title: 'Correcto',
                    text: "Bienvenido a SunnyPass!",
                    icon: 'info',
                }
                )
            } else {
                history.push('/calculate')
            }
        })
    }

    const calculate = (e) => {
        // /^[\d(?(?=\.)(\d))]$/
        // if (e.key === 0 || 1 || 2 || 3 || 4 || 5 || 6 || 7 || 8 || 9 || e.key === '.') {
        //     console.log('Entró a la condición del string: ' + e.key.toString());
        //     number1X = number1X + e.key.toString();
        //     number2X += e.key.toString();
        // }


        var num1 = parseFloat(e);
        var num2 = number2;
        var numI = num1;
        setSuma(num1 + num2);
        setResta(num1 - num2);
        setMultiplicacion(num1 * num2);
        setDivision(num1 / num2);
        setExponenciacion(num1 ** num2);
        setResiduo(num1 % num2);
        setIncremento(++numI);
        setDecremento(numI - 2);
        setAsignIgual(num2);
        setAsignSuma(num1 += num2);
        setAsignResta(num1 -= num2);
        setAsignMult(num1 *= num2);
        setAsignDiv(num1 /= num2);
        setAsignResiduo(num1 %= num2);
        setAsignCorrD(num1 <<= num2);
        setAsignCorrI(num1 >>= num2);
        setAsignCorrI2(num1 >>>= num2);
        setAsignAnd(num1 &= num2);
        setAsignNot(num1 ^= num2);
        setAsignOr(num1 |= num2);
        setAsignExp(num1 **= num2);
        num1 = parseFloat(e);
        num2 = number2;

        console.log('Calculate1 var e: ', e);
        setNumber1(num1);
        console.log('Type of number1' + typeof number1);
        console.log('number1 : ' + number1);
        console.log('Suma antes de calculate1: ' + suma);

        console.log('Suma después de calculate1: ' + suma);



    }
    const calculate2 = (e) => {
        // /^[\d(?(?=\.)(\d))]$/
        // if (e.key === 0 || 1 || 2 || 3 || 4 || 5 || 6 || 7 || 8 || 9 || e.key === '.') {
        //     console.log('Entró a la condición del string: ' + e.key.toString());
        //     number1X = number1X + e.key.toString();
        //     number2X += e.key.toString();
        // }

        var num2 = parseFloat(e);
        var num1 = number1;

        setSuma(num1 + num2);
        setResta(num1 - num2);
        setMultiplicacion(num1 * num2);
        setDivision(num1 / num2);
        setExponenciacion(num1 ** num2);
        setResiduo(num1 % num2);
        setAsignIgual(num2);
        setAsignSuma(num1 += num2);
        setAsignResta(num1 -= num2);
        setAsignMult(num1 *= num2);
        setAsignDiv(num1 /= num2);
        setAsignResiduo(num1 %= num2);
        setAsignCorrD(num1 <<= num2);
        setAsignCorrI(num1 >>= num2);
        setAsignCorrI2(num1 >>>= num2);
        setAsignAnd(num1 &= num2);
        setAsignNot(num1 ^= num2);
        setAsignOr(num1 |= num2);
        setAsignExp(num1 **= num2);

        // setIncremento(++numI2);
        // setDecremento(numD2 - 2);
        num2 = parseFloat(e);
        num1 = number1;
        console.log('Calculate1 var e: ', e);
        setNumber2(num2);
        console.log('Type of number2' + typeof number2);
        console.log('number2 : ' + number2);
        console.log('Suma antes de calculate2: ' + suma);

        console.log('Suma después de calculate2: ' + suma);




    }

    const makeObject = (e) => {
        console.log('Este es e: ' + e);
        switch (e.target.id) {

            case 'marca':
                //Declaraciones ejecutadas cuando el resultado de expresión coincide con el valor1
                console.log('Entré al caso de marca');
                setCarro({ ...carro, marca: e.target.value });
                setMarca(e.target.value);
                break;
            case 'compañia':
                //Declaraciones ejecutadas cuando el resultado de expresión coincide con el valor1
                console.log('Entré al caso de compañia');
                setCarro({ ...carro, compañia: e.target.value });
                setCompañia(e.target.value);
                break;
            case 'modelo':
                //Declaraciones ejecutadas cuando el resultado de expresión coincide con el valor1
                console.log('Entré al caso de modelo');
                setCarro({ ...carro, modelo: e.target.value });
                setModelo(e.target.value);
                break;
            case 'color':
                //Declaraciones ejecutadas cuando el resultado de expresión coincide con el valor1
                console.log('Entré al caso de color');
                setCarro({ ...carro, color: e.target.value });
                setColor(e.target.value);
                break;
            case 'nombre':
                //Declaraciones ejecutadas cuando el resultado de expresión coincide con el valor1
                console.log('Entré al caso de nombre');
                setCarro({ ...carro, nombre: e.target.value });
                setNombre(e.target.value);
                break;
            case 'apellido':
                //Declaraciones ejecutadas cuando el resultado de expresión coincide con el valor1
                console.log('Entré al caso de apellido');
                setCarro({ ...carro, apellido: e.target.value });
                setApellido(e.target.value);
                //console.log('Nombre', carro.nombre)
                break;
            case 'birth':
                //Declaraciones ejecutadas cuando el resultado de expresión coincide con el valor1
                console.log('Entré al caso de fecha');
                // setCarro({ ...carro, fecha: e.target.value });
                // setBirthday(e.target.value);
                console.log('Fecha', carro.fecha.toString());
                console.log('Fecha sola: ', birthday)
                d = birthday;
                setBirthD(d.toISOString());
                break;
            case 'argumento':
                //Declaraciones ejecutadas cuando el resultado de expresión coincide con el valor1
                console.log('Entré al caso de argumento');
                // setCarro({ ...carro, fecha: e.target.value });
                setArgumento(e.target.value);
                console.log('Argumento solo: ', argumento);
                break;
            case 'argumento2':
                //Declaraciones ejecutadas cuando el resultado de expresión coincide con el valor1
                console.log('Entré al caso de argumento 2');
                // setCarro({ ...carro, fecha: e.target.value });
                setArgumento2(e.target.value);
                console.log('Argumento 2 solo: ', argumento);
                break;
            default:
                console.log('Something else');


        }
        console.log('Make the object');
    }

    return (
        <>
            <div className='main'>
                <TopBar />
                <SideBar />

                <div className='contentX' style={{ overflowX: 'scroll', marginLeft: '-11vw' }}>

                    <div className='bg' style={{ width: '120%', backgroundSize: 'contain', backgroundRepeat: 'repeat', backgroundAttachment: 'scroll', backgroundPosition: 'center', height: '200vh' }}></div>
                    <div className='container1X' style={{ marginLeft: '1vw', marginTop: '57vh' }} >
                        <div style={{ color: 'white', backgroundColor: '#4B0082', height: '7.5vh', justifyContent: 'center', paddingTop: '1.5vh', paddingBotton: '-0.5vh', fontSize: '24px' }}>Ingrese los datos</div>
                        <div className='containerRow1X'>
                            <div style={{ display: 'flex' }}>
                                <FontAwesomeIcon icon={faSortNumericUp} style={{ marginLeft: '0vw', marginRight: '0.2vw', color: '4B0082' }} />
                                <i class="fas fa-user"></i>
                                <input
                                    id="number1"
                                    type="number"
                                    className="form-control"
                                    name="number1"
                                    placeholder="Escribe el Número 1"
                                    onChange={(e) => calculate(e.target.value)}
                                />
                                <FontAwesomeIcon icon={faSortNumericDown} style={{ marginLeft: '0vw', marginRight: '0.2vw', color: '4B0082' }} />

                            </div>
                        </div>

                        <div style={{ display: 'flex', justifyContent: 'space-between', width: '80 %', marginLeft: '0%' }}>
                            <FontAwesomeIcon icon={faPlus} style={{ marginLeft: '4vw', color: '#4B0082' }} />
                            <FontAwesomeIcon icon={faMinus} style={{ color: '4B0082' }} />
                            <FontAwesomeIcon icon={faAsterisk} style={{ color: '4B0082' }} />
                            <FontAwesomeIcon icon={faDivide} style={{ marginRight: '4vw', color: '4B0082' }} />
                        </div>

                        <div className='containerRow1X'>
                            <div style={{ display: 'flex' }}>
                                <FontAwesomeIcon icon={faSortNumericDown} style={{ marginLeft: '0vw', marginRight: '0.2vw', color: '4B0082' }} />
                                <i class="fas fa-key"></i>
                                <input
                                    id="number2"
                                    type="number"
                                    className="form-control"
                                    name="number2"
                                    placeholder="Escribe el Número 2"
                                    onChange={(e) => calculate2(e.target.value)}
                                />
                                <FontAwesomeIcon icon={faSortNumericUp} style={{ marginLeft: '0vw', marginRight: '0.2vw', color: '4B0082' }} />
                            </div>
                        </div>

                        <div className='containerRow1X'>
                            <div style={{ display: 'flex' }}>
                                <FontAwesomeIcon icon={faCar} style={{ marginLeft: '0vw', marginRight: '0.2vw', color: '4B0082' }} />
                                <i class="fas fa-user"></i>
                                <input
                                    id="marca"
                                    type="text"
                                    className="form-control"
                                    name="marca"
                                    placeholder="Marca"
                                    onChange={(e) => makeObject(e)}
                                />
                                <FontAwesomeIcon icon={faCar} style={{ marginLeft: '0vw', marginRight: '0.2vw', color: '4B0082' }} />

                            </div>
                        </div>
                        <div className='containerRow1X'>
                            <div style={{ display: 'flex' }}>
                                <FontAwesomeIcon icon={faCar} style={{ marginLeft: '0vw', marginRight: '0.2vw', color: '4B0082' }} />
                                <i class="fas fa-user"></i>
                                <input
                                    id="compañia"
                                    type="text"
                                    className="form-control"
                                    name="compañia"
                                    placeholder="Compañia"
                                    onChange={(e) => makeObject(e)}
                                />
                                <FontAwesomeIcon icon={faCar} style={{ marginLeft: '0vw', marginRight: '0.2vw', color: '4B0082' }} />

                            </div>
                        </div>
                        <div className='containerRow1X'>
                            <div style={{ display: 'flex' }}>
                                <FontAwesomeIcon icon={faCar} style={{ marginLeft: '0vw', marginRight: '0.2vw', color: '4B0082' }} />
                                <i class="fas fa-user"></i>
                                <input
                                    id="modelo"
                                    type="number"
                                    className="form-control"
                                    name="modelo"
                                    placeholder="Modelo"
                                    onChange={(e) => makeObject(e)}
                                />
                                <FontAwesomeIcon icon={faCar} style={{ marginLeft: '0vw', marginRight: '0.2vw', color: '4B0082' }} />

                            </div>
                        </div>
                        <div className='containerRow1X'>
                            <div style={{ display: 'flex' }}>
                                <FontAwesomeIcon icon={faCar} style={{ marginLeft: '0vw', marginRight: '0.2vw', color: '4B0082' }} />
                                <i class="fas fa-user"></i>
                                <input
                                    id="color"
                                    type="text"
                                    className="form-control"
                                    name="color"
                                    placeholder="Color"
                                    onChange={(e) => makeObject(e)}
                                />
                                <FontAwesomeIcon icon={faCar} style={{ marginLeft: '0vw', marginRight: '0.2vw', color: '4B0082' }} />

                            </div>
                        </div>
                        <div className='containerRow1X'>
                            <div style={{ display: 'flex' }}>
                                <FontAwesomeIcon icon={faCar} style={{ marginLeft: '0vw', marginRight: '0.2vw', color: '4B0082' }} />
                                <i class="fas fa-user"></i>
                                <input
                                    id="nombre"
                                    type="text"
                                    className="form-control"
                                    name="nombre"
                                    placeholder="Nombre del dueño"
                                    onChange={(e) => makeObject(e)}
                                />
                                <FontAwesomeIcon icon={faCar} style={{ marginLeft: '0vw', marginRight: '0.2vw', color: '4B0082' }} />

                            </div>
                        </div>

                        <div className='containerRow1X'>
                            <div style={{ display: 'flex' }}>
                                <FontAwesomeIcon icon={faCar} style={{ marginLeft: '0vw', marginRight: '0.2vw', color: '4B0082' }} />
                                <i class="fas fa-user"></i>
                                <input
                                    id="apellido"
                                    type="text"
                                    className="form-control"
                                    name="apellido"
                                    placeholder="Apellido del dueño"
                                    onChange={(e) => makeObject(e)}
                                />
                                <FontAwesomeIcon icon={faCar} style={{ marginLeft: '0vw', marginRight: '0.2vw', color: '4B0082' }} />

                            </div>
                        </div>
                        <div className='containerRow1X'>
                            <div style={{ display: 'flex' }}>
                                <FontAwesomeIcon icon={faMagic} style={{ marginLeft: '0vw', marginRight: '0.2vw', color: '4B0082' }} />
                                <i class="fas fa-user"></i>
                                <input
                                    id="argumento"
                                    type="text"
                                    className="form-control"
                                    name="argumento"
                                    placeholder="Argumento para funciones"
                                    onChange={(e) => makeObject(e)}
                                />
                                <FontAwesomeIcon icon={faMagic} style={{ marginLeft: '0vw', marginRight: '0.2vw', color: '4B0082' }} />

                            </div>
                        </div>
                        <div className='containerRow1X'>
                            <div style={{ display: 'flex' }}>
                                <FontAwesomeIcon icon={faMagic} style={{ marginLeft: '0vw', marginRight: '0.2vw', color: '4B0082' }} />
                                <i class="fas fa-user"></i>
                                <input
                                    id="argumento2"
                                    type="text"
                                    className="form-control"
                                    name="argumento2"
                                    placeholder="Argumento 2 para funciones"
                                    onChange={(e) => makeObject(e)}
                                />
                                <FontAwesomeIcon icon={faMagic} style={{ marginLeft: '0vw', marginRight: '0.2vw', color: '4B0082' }} />

                            </div>
                        </div>
                        <div className='containerRow1X'>
                            <div style={{ display: 'flex', marginLeft: '2vw' }}>
                                <FontAwesomeIcon icon={faBirthdayCake} style={{ marginLeft: '-2vw', marginRight: '0.2vw', color: 'black' }} />
                                <i class="fas fa-user"></i>
                                <Calendar
                                    id="birth"
                                    type="text"
                                    className="form-control"
                                    name="birth"
                                    placeholder="Fecha de Compra"
                                    value={formattedDate}
                                    birthday={birthday}
                                    setBirthday={setBirthday}
                                // onChange={data => makeObject(data)}
                                />
                                <FontAwesomeIcon icon={faBirthdayCake} style={{ marginLeft: '0vw', marginRight: '0.2vw', color: 'black' }} />

                            </div>
                        </div>


                        <FontAwesomeIcon icon={faEquals} style={{ marginLeft: '0vw', marginRight: '0vw', color: 'black', justifySelf: 'center', width: '100%' }} />

                        <div className='containerRow1X'>
                            <Button style={{ borderRadius: '5px' }} className='buttonA' onClick={e => (swalAlert(e))}>
                                <span class="fas fa-user" style={{ marginRight: '0.3vw' }}></span>
                            Resultado
                            </Button>
                            <Link to='/'>
                                <Button style={{ borderRadius: '5px' }} className='buttonA'>VOLVER</Button>
                            </Link>
                        </div>
                    </div>
                    <div style={{ marginTop: '-1.7vw', marginLeft: '-22vw', marginBottom: '-1.7vw' }}>NOTA: Solo saca incremento y decremento del número 1, por lógicas razones</div>
                    <div style={{ display: 'flex', width: '70%', position: 'absolute', marginTop: '-30%', marginLeft: '27%', justifyContent: 'space-between', backgroundColor: '#4B0082' }}>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '70%', marginLeft: 'l5%', justifyContent: 'space-between' }}>
                            <div>SUMA</div>
                            <div style={{ backgroundColor: 'white', color: '#4B0082' }}>{suma}</div>
                            {/* <div placeholder={suma}></div> */}
                        </div>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '70%', marginLeft: 'l5%', justifyContent: 'space-between' }}>
                            <div>RESTA</div>
                            <div style={{ backgroundColor: 'white', color: '#4B0082' }}>{resta}</div>
                        </div>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '70%', marginLeft: 'l5%', justifyContent: 'space-between' }}>
                            <div>MULTIPLICACION</div>
                            <div style={{ backgroundColor: 'white', color: '#4B0082' }}>{multiplicacion}</div>
                        </div>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '70%', marginLeft: 'l5%', justifyContent: 'space-between' }}>
                            <div>DIVISION</div>
                            <div style={{ backgroundColor: 'white', color: '#4B0082' }}>{division}</div>
                        </div>
                    </div>
                    <div style={{ display: 'flex', width: '70%', position: 'absolute', marginTop: '-23%', marginLeft: '27%', marginBottom: '0.2vw', justifyContent: 'space-between', backgroundColor: '#4B0082' }}>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '70%', marginLeft: 'l5%', justifyContent: 'space-between' }}>
                            <div>EXPONENCIACION</div>
                            <div style={{ backgroundColor: 'white', color: '#4B0082' }}>{exponenciacion}</div>
                            {/* <div placeholder={suma}></div> */}
                        </div>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '70%', marginLeft: 'l5%', justifyContent: 'space-between' }}>
                            <div>RESIDUO</div>
                            <div style={{ backgroundColor: 'white', color: '#4B0082' }}>{residuo}</div>
                        </div>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '70%', marginLeft: 'l5%', justifyContent: 'space-between' }}>
                            <div>INCREMENTO</div>
                            <div style={{ backgroundColor: 'white', color: '#4B0082' }}>{incremento}</div>
                        </div>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '70%', marginLeft: 'l5%', justifyContent: 'space-between' }}>
                            <div>DECREMENTO</div>
                            <div style={{ backgroundColor: 'white', color: '#4B0082' }}>{decremento}</div>
                        </div>
                    </div>
                    <div style={{ display: 'flex', width: '70%', position: 'absolute', marginTop: '-16%', marginLeft: '27%', marginBottom: '0.2vw', justifyContent: 'space-between', backgroundColor: '#4B0082' }}>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '70%', marginLeft: 'l5%', justifyContent: 'space-between' }}>
                            <div>A. IGUAL</div>
                            <div style={{ backgroundColor: 'white', color: '#4B0082' }}>{asignIgual}</div>
                            {/* <div placeholder={suma}></div> */}
                        </div>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '70%', marginLeft: 'l5%', justifyContent: 'space-between' }}>
                            <div>A. SUMA</div>
                            <div style={{ backgroundColor: 'white', color: '#4B0082' }}>{asignSuma}</div>
                        </div>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '70%', marginLeft: 'l5%', justifyContent: 'space-between' }}>
                            <div>A. RESTA</div>
                            <div style={{ backgroundColor: 'white', color: '#4B0082' }}>{asignResta}</div>
                        </div>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '70%', marginLeft: 'l5%', justifyContent: 'space-between' }}>
                            <div>A. MULT</div>
                            <div style={{ backgroundColor: 'white', color: '#4B0082' }}>{asignMult}</div>
                        </div>
                    </div>
                    <div style={{ display: 'flex', width: '70%', position: 'absolute', marginTop: '-9%', marginLeft: '27%', marginBottom: '0.2vw', justifyContent: 'space-between', backgroundColor: '#4B0082' }}>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '70%', marginLeft: 'l5%', justifyContent: 'space-between' }}>
                            <div>A. DIVISION</div>
                            <div style={{ backgroundColor: 'white', color: '#4B0082' }}>{asignDiv}</div>
                            {/* <div placeholder={suma}></div> */}
                        </div>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '70%', marginLeft: 'l5%', justifyContent: 'space-between' }}>
                            <div>A. RESIDUO</div>
                            <div style={{ backgroundColor: 'white', color: '#4B0082' }}>{asignResiduo}</div>
                        </div>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '70%', marginLeft: 'l5%', justifyContent: 'space-between' }}>
                            <div>A. CORR- D.</div>
                            <div style={{ backgroundColor: 'white', color: '#4B0082' }}>{asignCorrD}</div>
                        </div>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '70%', marginLeft: 'l5%', justifyContent: 'space-between' }}>
                            <div>A. CORR. I.</div>
                            <div style={{ backgroundColor: 'white', color: '#4B0082' }}>{asignCorrI}</div>
                        </div>
                    </div>
                    <div style={{ display: 'flex', width: '70%', position: 'absolute', marginTop: '-2%', marginLeft: '27%', marginBottom: '0.2vw', justifyContent: 'space-between', backgroundColor: '#4B0082' }}>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '70%', marginLeft: 'l5%', justifyContent: 'space-between' }}>
                            <div>A. CORR. I2</div>
                            <div style={{ backgroundColor: 'white', color: '#4B0082' }}>{asignCorrI2}</div>
                            {/* <div placeholder={suma}></div> */}
                        </div>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '70%', marginLeft: 'l5%', justifyContent: 'space-between' }}>
                            <div>A. AND</div>
                            <div style={{ backgroundColor: 'white', color: '#4B0082' }}>{asignAnd}</div>
                        </div>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '70%', marginLeft: 'l5%', justifyContent: 'space-between' }}>
                            <div>A. NOT</div>
                            <div style={{ backgroundColor: 'white', color: '#4B0082' }}>{asignNot}</div>
                        </div>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '70%', marginLeft: 'l5%', justifyContent: 'space-between' }}>
                            <div>A. OR</div>
                            <div style={{ backgroundColor: 'white', color: '#4B0082' }}>{asignOr}</div>
                        </div>
                    </div>
                    <div style={{ display: 'flex', width: '70%', position: 'absolute', marginTop: '5%', marginLeft: '27%', marginBottom: '0.2vw', justifyContent: 'space-between', backgroundColor: '#4B0082' }}>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '70%', marginLeft: 'l5%', justifyContent: 'space-between' }}>
                            <div>A. EXP</div>
                            <div style={{ backgroundColor: 'white', color: '#4B0082' }}>{asignExp}</div>
                            {/* <div placeholder={suma}></div> */}
                        </div>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '70%', marginLeft: 'l5%', justifyContent: 'space-between' }}>
                            <div>MARCA</div>
                            <div style={{ backgroundColor: 'white', color: '#4B0082' }}>{`String: ${marca} Extensión: ${marca.length}`}</div>
                        </div>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '70%', marginLeft: 'l5%', justifyContent: 'space-between' }}>
                            <div>COMPAÑIA</div>
                            <div style={{ backgroundColor: 'white', color: '#4B0082' }}>{`String: ${compañia} Extensión: ${compañia.length}`}</div>
                        </div>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '70%', marginLeft: 'l5%', justifyContent: 'space-between' }}>
                            <div>MODELO</div>
                            <div style={{ backgroundColor: 'white', color: '#4B0082' }}>{`String: ${modelo} Extensión: ${modelo.length}`}</div>
                        </div>
                    </div>
                    <div style={{ display: 'flex', width: '70%', marginLeft: '27%', position: 'absolute', marginTop: '12%', marginBottom: '0.2vw', justifyContent: 'space-between', backgroundColor: '#4B0082' }}>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '70%', marginLeft: 'l5%', justifyContent: 'space-between' }}>
                            <div>COLOR</div>
                            <div style={{ backgroundColor: 'white', color: '#4B0082' }}>{`String: ${color} Extensión: ${color.length}`}</div>
                            {/* <div placeholder={suma}></div> */}
                        </div>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '70%', marginLeft: 'l5%', justifyContent: 'space-between' }}>
                            <div>NOMBRE</div>
                            <div style={{ backgroundColor: 'white', color: '#4B0082' }}>{`String: ${nombre} Extensión: ${nombre.length}`}</div>
                        </div>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '70%', marginLeft: 'l5%', justifyContent: 'space-between' }}>
                            <div>APELLIDO</div>
                            <div style={{ backgroundColor: 'white', color: '#4B0082' }}>{`String: ${apellido} Extensión: ${apellido.length}`}</div>
                        </div>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '70%', marginLeft: 'l5%', justifyContent: 'space-between' }}>
                            <div style={{ fontSize: '12px' }}>CONCATENADO DEL NOMBRE</div>
                            <div style={{ backgroundColor: 'white', color: '#4B0082' }}>{nombre + ' ' + apellido}</div>
                        </div>
                    </div>
                    <div style={{ display: 'flex', width: '70%', marginLeft: '27%', position: 'absolute', marginTop: '19%', marginBottom: '0.2vw', justifyContent: 'space-between', backgroundColor: '#4B0082' }}>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '70%', marginLeft: 'l5%', justifyContent: 'space-between' }}>
                            <div style={{ fontSize: '12px' }}>REPLACE EN NOMBRE</div>
                            <div style={{ backgroundColor: 'white', color: '#4B0082' }}>{nombre.replace(argumento, argumento2)}</div>
                            {/* <div placeholder={suma}></div> */}
                        </div>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '70%', marginLeft: 'l5%', justifyContent: 'space-between' }}>
                            <div style={{ fontSize: '12px' }}>BÚSCAR POSICIÓN EN APELLIDO</div>
                            <div style={{ backgroundColor: 'white', color: '#4B0082' }}>{apellido.search(argumento)}</div>
                        </div>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '70%', marginLeft: 'l5%', justifyContent: 'space-between' }}>
                            <div style={{ fontSize: '12px' }}>EXTRAER PARTE DE NOMBRE</div>
                            <div style={{ backgroundColor: 'white', color: '#4B0082' }}>{nombre.slice(Number(argumento), Number(argumento2))}</div>
                        </div>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '70%', marginLeft: 'l5%', justifyContent: 'space-between' }}>
                            <div style={{ fontSize: '12px' }}>A MAYÚSCULAS EL APELLIDO</div>
                            <div style={{ backgroundColor: 'white', color: '#4B0082' }}>{apellido.toUpperCase()}</div>
                        </div>
                    </div>
                    <div style={{ display: 'flex', width: '70%', marginLeft: '27%', position: 'absolute', marginTop: '26%', marginBottom: '0.2vw', justifyContent: 'space-between', backgroundColor: '#4B0082' }}>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '70%', marginLeft: 'l5%', justifyContent: 'space-between' }}>
                            <div style={{ fontSize: '12px' }}>CONCAT ARG1 Y ARG2</div>
                            <div style={{ backgroundColor: 'white', color: '#4B0082' }}>{argumento.concat(" ", argumento2)}</div>
                            {/* <div placeholder={suma}></div> */}
                        </div>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '70%', marginLeft: 'l5%', justifyContent: 'space-between' }}>
                            <div style={{ fontSize: '12px' }}>PADDING FINAL APELLIDO</div>
                            <div style={{ backgroundColor: 'white', color: '#4B0082' }}>{apellido.padEnd(number1, argumento)}</div>
                        </div>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '70%', marginLeft: 'l5%', justifyContent: 'space-between' }}>
                            <div>CHAR AT NOMBRE</div>
                            <div style={{ backgroundColor: 'white', color: '#4B0082' }}>{nombre.charAt(Number(argumento))}</div>
                        </div>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '70%', marginLeft: 'l5%', justifyContent: 'space-between' }}>
                            <div style={{ fontSize: '12px' }}>CHAR CODE AT NOMBRE</div>
                            <div style={{ backgroundColor: 'white', color: '#4B0082' }}>{nombre.charCodeAt(Number(argumento))}</div>
                        </div>

                    </div>
                    <div style={{ display: 'flex', width: '70%', marginLeft: '27%', position: 'absolute', marginTop: '33%', marginBottom: '0.2vw', justifyContent: 'space-between', backgroundColor: '#4B0082' }}>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '70%', marginLeft: 'l5%', justifyContent: 'space-between' }}>
                            <div style={{ fontSize: '12px' }}>SPLIT ARG1 EN ARG2</div>
                            <div style={{ backgroundColor: 'white', color: '#4B0082' }}>{argumento.split(argumento2)}</div>
                            {/* <div placeholder={suma}></div> */}
                        </div>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '70%', marginLeft: 'l5%', justifyContent: 'space-between' }}>
                            <div style={{ fontSize: '10px' }}>TO EXPONENTIAL NUMERO1 USANDO NUMERO2 DE ARG</div>
                            <div style={{ backgroundColor: 'white', color: '#4B0082' }}>{number1.toExponential(number2)}</div>
                        </div>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '70%', marginLeft: 'l5%', justifyContent: 'space-between' }}>
                            <div style={{ fontSize: '10px' }}>TO FIXED NUMERO 1 USANDO NUMERO2 DE ARG</div>
                            <div style={{ backgroundColor: 'white', color: '#4B0082' }}>{number1.toFixed(number2)}</div>
                        </div>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '70%', marginLeft: 'l5%', justifyContent: 'space-between' }}>
                            <div>NUMBER DE FECHA</div>
                            <div style={{ backgroundColor: 'white', color: '#4B0082' }}>{Number(birthday)}</div>
                        </div>

                    </div>
                    <div style={{ display: 'flex', width: '70%', marginLeft: '27%', position: 'absolute', marginTop: '40%', marginBottom: '0.2vw', justifyContent: 'space-between', backgroundColor: '#4B0082' }}>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '70%', marginLeft: 'l5%', justifyContent: 'space-between' }}>
                            <div>NUMBER MAX VALUE</div>
                            <div style={{ backgroundColor: 'white', color: '#4B0082' }}>{Number.MAX_VALUE}</div>
                            {/* <div placeholder={suma}></div> */}
                        </div>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '70%', marginLeft: 'l5%', justifyContent: 'space-between' }}>
                            <div style={{ fontSize: '12px' }}>RAIZ CUADRADA AL ARGUMENTO 1</div>
                            <div style={{ backgroundColor: 'white', color: '#4B0082' }}>{Math.sqrt(Number(argumento))}</div>
                        </div>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '70%', marginLeft: 'l5%', justifyContent: 'space-between' }}>
                            <div style={{ fontSize: '12px' }}>RAIZ CUADRADA AL ARGUMENTO 2</div>
                            <div style={{ backgroundColor: 'white', color: '#4B0082' }}>{Math.sqrt(Number(argumento))}</div>
                        </div>
                        <div style={{ display: 'flex', flexDirection: 'column', width: '70%', marginLeft: 'l5%', justifyContent: 'space-between' }}>
                            <div>MATH RANDOM</div>
                            <div style={{ backgroundColor: 'white', color: '#4B0082' }}>{Math.floor(Math.random() * (argumento2 - argumento)) + argumento2}</div>
                        </div>
                    </div>
                    <div style={{ display: 'flex', width: '19%', position: 'absolute', marginTop: '-40%', marginLeft: '35%', justifyContent: 'space-between', backgroundImage: 'linear-gradient(to right, #4B0082, #DA70D6)', fontSize: '30px' }}>CALCULADORA</div>

                </div>
            </div>
            <Footer />
        </>
    );
}

export default Form;