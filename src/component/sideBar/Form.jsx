import React from 'react';
import { Button } from 'reactstrap';
import { Link } from 'react-router-dom';


const Form = () => {

    return (
        <div className='menu'>
            {/* <Link to='/dashboard/agenda'>
                <Button className='buttonA'>Agenda</Button>
            </Link> */}
            <div style={{
                marginLeft: '0vw',
                boxShadow: '0px 0px 10px rgba(0, 0, 0, .25)',
                textDecoration: 'underline overline',

            }}>ALTA</div>
            <Link to='/'>
                <Button className='buttonA' style={{ paddingLeft: '0vw', paddingRight: '0.5vw', marginBottom: '1vh', width: '100%' }}>
                    <span class="fas fa-user-graduate" style={{ marginRight: '0.3vw' }}></span>
                    INICIO
                </Button>
            </Link>

            <Link to='/register'>
                <Button className='buttonA' style={{ paddingLeft: '0vw', paddingRight: '0.5vw', marginBottom: '1vh', width: '100%' }}>
                    <span class="fas fa-user-graduate" style={{ marginRight: '0.3vw' }}></span>
                    REGISTRO
                </Button>
            </Link>
            <Link to='/getApp'>
                <Button className='buttonA' style={{ paddingLeft: '0vw', paddingRight: '0.5vw', width: '100%' }}>
                    <span class="fas fa-chess-king" style={{ marginRight: '0.3vw' }}></span>
                    OFERTAS
                </Button>
            </Link>

            <Link to='/login'>
                <Button className='buttonA' style={{ paddingLeft: '0vw', paddingRight: '0.5vw', width: '100%' }}>
                    <span class="fas fa-guitar" style={{ marginRight: '0.3vw' }}></span>
                    SESION
                </Button>
            </Link>

            <div style={{
                marginLeft: '0vw',
                boxShadow: '0px 0px 10px rgba(0, 0, 0, .25)',
                textDecoration: 'underline overline',
            }}>GESTIÓN</div>
            <Link to='/options'>
                <Button className='buttonA' style={{ paddingLeft: '0vw', paddingRight: '0.5vw', width: '100%' }}>
                    <span class="fas fa-user-graduate" style={{ marginRight: '0.3vw' }}></span>
                    OPCIONES
                </Button>
            </Link>
            <Link to='/aboutUs'>
                <Button className='buttonA' style={{ paddingLeft: '0vw', paddingRight: '0.5vw', width: '100%' }}>
                    <span class="fas fa-graduation-cap" style={{ marginRight: '0.3vw' }}></span>
                    NOSOTROS
                </Button>
            </Link>
            <Link to='/contact'>
                <Button className='buttonA' style={{ paddingLeft: '0vw', paddingRight: '0.5vw', width: '100%' }}>
                    <span class="fas fa-chess-queen" style={{ marginRight: '0.3vw' }}></span>
                    CONTACTO
                </Button>
            </Link>
            <Link to='/sugest'>
                <Button className='buttonA' style={{ paddingLeft: '0vw', paddingRight: '0.5vw', width: '100%' }}>
                    <span class="fas fa-palette" style={{ marginRight: '0.3vw' }}></span>
                    SUGERENCIAS
                </Button>
            </Link>




            {/* <Link to='/dashboard/admin/student'>
                <Button className='buttonA'>Usuarios</Button>
            </Link> */}
        </div>
    );
}

export default Form;